var mouseControlApp = angular.module('mouseControl', ['ngTouch']);

mouseControlApp.directive('mouseControl', function() {
    return {
        restrict: 'EA',
        templateUrl: 'Templates/mouse_control_template.html',
        controller: 'mouseCtrl'
    }
});