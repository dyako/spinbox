var steamApp = angular.module('steam', []);

steamApp.directive('steamApp', function() {
    return {
        restrict: 'EA',
        templateUrl: 'Templates/steam_template.html',
        controller: 'steamCtrl'
    }
});