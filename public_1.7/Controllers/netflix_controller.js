netflixApp.controller('netflixCtrl', function($scope, $http) {
    
    $scope.hideOptions = true;
    
    $scope.switchHideOptions = function()
    {
        if($scope.hideOptions) {
            $scope.hideOptions = false;   
        }
        else {
            $scope.hideOptions = true;   
        }
    }
    
    $scope.launchNetflix = function() {
        console.log("boton lanzar netflix");
        $http.post('/netflix').success(function(data) {
            console.log("lanzar netflix - " + data);
        });
    }
     
});