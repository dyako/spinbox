var os          = require('os');
var logger      = require('./modules/Logger.js');
var Mongodb     = require('./modules/Mongodb_tools.js');
var Command     = require('./modules/Command.js');
var Gamepads    = require('./modules/Gamepad_tools.js');
var wifi        = require('./modules/Wifi_tools.js');
var audio       = require('./modules/Audio_tools.js');
var retroarch   = require('./modules/Retroarch.js');
var fs          = require('fs');

var spinboxDb   = new Mongodb('spinbox');
var command     = new Command();

/*********************************/
/**** ROUTER DE LA APLICACION ****/
/*********************************/

function router(app, io) {

    //var gamepads  = new Gamepads(io).scan();
    
    /*+++++++++++++++++*/
    /*     SOCKET      */
    /*******************/

    // Evento para la conexion de un usuario al control remoto mediante un socket.
    io.on('connection', function(socket) {
        logger.log('debug', 'User connected');

        socket.on('disconnect', function() {
            logger.log('debug', 'User disconected');
        });

        /* REMOTE MOUSE CONTROL */
        socket.on('moveMouse', function(data) {
            logger.log('debug', 'Move mouse: ' + data.x + ', ' + data.y);
            //robot.moveMouse(data);
        });

        socket.on('clickMouse', function(data) {
            logger.log('debug', 'click Mouse');
            //robot.clickMouse();
        });

        socket.on('scrollMouse', function(data) {
            logger.log('debug', 'scroll Mouse');
            robot.scrollMouse(data);
        });

        socket.on('sendText', function(data) {
            logger.log('debug', 'Sending text');
            robot.typeText(data);
        });

        socket.on('sendButton', function(data) {
            logger.log('debug', 'Sending Button');
            robot.pressKey(data.button);
        });
    });
    
    
    /*************************************************************/
    /***** APLICACIONES ******************************************/
    /*************************************************************/
    
    // Get predeterminado.
    app.get("/", function(req, res, next) {
        res.sendFile(__dirname + '/public_3/TV/index.html');   
    });

    app.get("/Remote", function(req, res, next) {
        res.sendFile(__dirname + '/public_3/Remote/index.html');   
    });

    // Get de la pantalla de la TV.
    app.get("/TV", function(req, res, next) {
        res.sendFile(__dirname + '/public_3/TV/index.html');   
    });
    
    // Get de la pantalla de la TV.
    app.get("/TV-2.0", function(req, res, next) {
        res.sendFile(__dirname + '/public_2/TV/index.html');   
    });

    app.get("/TV-1.7", function(req, res, next) {
        res.sendFile(__dirname + '/public_1.7/RPI_index.html');   
    });
    /*************************************************************/
    
    
    
    /*************************************************************/
    /***** FUNCIONES DE SISTEMA **********************************/
    /*************************************************************/
    
    // Devuelve la IP del servidor a la pagina de la TV.
    app.get("/getIp", function(req, res, next) {
        var interfaces  = os.networkInterfaces();
        var addresses = [];
        for (var k in interfaces) {
            for(var k2 in interfaces[k]) {
                var address = interfaces[k][k2];
                if(address.family === 'IPv4' && !address.internal) {
                    addresses.push(address.address);
                }
            }
        }

        logger.log('info', "Given IP: " + addresses);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify({ address: addresses }));
    });
    
    // Apaga el dispositivo ejecutando el comando de administrador.
    app.get("/powerOff", function(req, res, next) {
        logger.log("info", "Shutting down system");
        command.run("sudo shutdown now", function(error, stdout, stderr) {
            if(stderr == "") { res.send(JSON.stringify({error: 0})); }
            else { res.send(JSON.stringify({error: 1})); }
        });
    });


    // Reinicia el dispositivo ejecutando el comando de administrador.
    app.get("/restartSystem", function(req, res, next) {
        logger.log("info", "Restarting system");
        command.run("sudo reboot now", function(error, stdout, stderr) {
            if(stderr == "") { res.send({error: 0}); }
            else { res.send({error: 1}); }
        });
    });


    // Cierra todas las aplicaciones y recarga el explorador con la app de TV.
    app.get("/goHome", function(req, res, next) {
        logger.log("info", "Going back main menu.");

        command.run('pidof kodi.bin', function(stdout) {
            var pidKodi = stdout;
            if(pidKodi != 0) { command.run('sudo kill ' + pidKodi); }
        });

        command.run('pidof emulationstation', function(stdout) {
            var pidEmulationstation = stdout;
            if(pidEmulationstation != 0) { command.run('sudo kill ' + pidEmulationstation); }
        });

        command.run('pidof moonlight', function(stdout) {
            var pidMoonlight = stdout;
            if(pidMoonlight != 0) { command.run('sudo kill ' + pidMoonlight); }
        });

        command.run('pidof xinit', function(stdout) {
            var pidXinit = stdout;
            if(pidXinit != 0) { command.run('sudo kill ' + pidXinit); }
        });


        gamepads.setBlocked(false);
        res.send("OK");
    });
    /*********************************************************************/
    
    
    
    /*+++++++++++++++++**************************************************/
    /***** LANZAMIENTO DE APPS ******************************************/
    /********************************************************************/

    // Ejecuta el programa Moonlight desde la pagina de control.
    app.post("/moonlight/launchProgram", function(req, res, next) {
        logger.log("info", "Launching Steam");
        saveConfig('steam_config.json', req.body);

        var options = req.body;
        var mCommand = 'sudo moonlight stream -' + options.resolution + " -" + options.fps + "fps " + options.remoteIP;

        io.emit('launchAnimation', { idElement: "steam-launch-image" });

        command.run(mCommand, function() {
            gamepads.setBlocked(false);
        });

        gamepads.setBlocked(true);
        res.send("OK");
    });

    // Ejecuta el programa Kodi desde la pagina de control.
    app.post("/kodi", function(req, res, next) {
         logger.log("info", "Launching Kodi");

        // Ejecuta la animacion en la pantalla de la TV
        //io.emit('launchAnimation', { idElement: "kodi-launch-image" });
        command.run('kodi-standalone', function() {
            //gamepads.setBlocked(false);
        });

        //gamepads.setBlocked(true);
        res.send("OK");

    });

    //// Ejecuta el programa Retropie desde la pagina de control.
    //app.post("/retropie", function(req, res, next) {
    //    console.log("Lanzando Retropie");
    //    
    //    var pidXinit = 0;
    //    
    //    console.log(process.env);
    //    
    //    // Ejecuta la animacion en la pantalla de la TV
    //    io.emit('launchAnimation', { idElement: "retropie-launch-image" });
    //    
    //    exec('pidof xinit', function(error, stdout, stderr) {
    //        console.log('stdout: ' + stdout);
    //        console.log('stderr: ' + stderr);
    //        
    //        pidXinit = stdout;
    //        if(pidXinit != 0) {
    //            exec('sudo kill ' + pidXinit, function(error, stdout, stderr) {
    //                console.log('stdout: ' + stdout);
    //                console.log('stderr: ' + stderr);
    //
    //
    //                setTimeout(function() {
    //                    // Tiempo de margen para que se elimine el proceso y se lance
    //
    //                    exec('emulationstation', function(error, stdout, stderr) {
    //                    console.log('stdout: ' + stdout);
    //                    console.log('stderr: ' + stderr);
    //                    if (error !== null) {
    //                        console.log('exec error: ' + error);
    //                    }
    //                    reloadApp();        // Relanza la app de la TV
    //                    gamepads.setBlocked(false);
    //                });
    //                }, 1000);
    //
    //                
    //                if (error !== null) {
    //                    console.log('exec error: ' + error);
    //                }
    //            });     
    //        }
    //        if (error !== null) {
    //            console.log('exec error: ' + error);
    //        }
    //    });
    //
    //    gamepads.setBlocked(true);
    //    res.send("OK");
    //
    //});
    /**********************************************************/

    

    /*+++++++++++++++++**************************************/
    /*****    RETROARCH    **********************************/
    /********************************************************/

    // Lista los archivos de un path.
    app.post('/retroarch/listRoms', function(req, res, next) {

        var platform = req.body.platform;
        var roms = retroarch.listRoms(platform);

        logger.log("debug", roms);
        res.send(JSON.stringify(roms));
    });


    app.post('/retroarch/launchRom', function(req, res, next) {
        var platform = req.body.platform;
        var rom = req.body.rom;

        // Crea el archivo de configuracion de los gamepads que estan conectados.
        var config = (gamepads.n != 0) ? ("--appendconfig " + gamepads.createRetroarchFile(__dirname + "/configs/")) : "";

        retroarch.launchRom(platform, rom, config, function() {
            gamepads.setBlocked(false);
        });

        gamepads.setBlocked(true);
        res.send('OK');

    });
    /*+++++++++++++++++*****************************************/
    
    
    
    /*+++++++++++++++++*****************************************/
    /*****    DB TOOLS   ***************************************/
    /*+++++++++++++++++*****************************************/

    app.post('/db/getDocuments', function(req, res, next) {
        var collection = req.body.collection;
        var filter = req.body.filter;
        var fields = {"_id": false};
        logger.log('debug', 'Getting ' + collection + ' from db with ' + JSON.stringify(filter) + ' filter');

        spinboxDb.find(collection, filter, fields, "", 0, function(docs) {
            res.send(docs);
        });
    });

    app.post("/emulators/toggleTop", function(req, res, next) {
        var data = req.body.filter; 

        var filter = {id: data.id};
        var update = {top: data.top};
        logger.log('debug', 'Updating ' + data.id + ' emulator from db with "' + data.top + '" top value');

        spinboxDb.update("emulators", filter, update, function(state) {
            var response = {error: 0};
            if(state.ok == 0) {
                response = {error: 1};
            }
            res.send(response);
        });
    });

    app.post("/roms/getTopRoms", function(req, res, next) {
        var data = req.body.filter; 

        var filter = {platform: data.platform};
        var fields = {"_id": false};
        var order = [['playedTimes', 'desc']];
        var limit = 5;
        logger.log('debug', 'Getting ' + data.platform + ' top roms from db.');

        spinboxDb.find("roms", filter, fields, order, limit, function(docs) {
            if(docs.length < 1) {
                docs = null;
            }
            res.send(docs);
        });
    });
    
    app.post("/roms/addRoms", function(req, res, next) {
        var data = req.body; 
        var platform = data.platform;
        var roms = JSON.parse(data.roms);
        
        var error = 0;
        
        roms.forEach(function(item, index) {
            var name = item.split(".")[0];
            var filter = {name: name, file: item, platform: platform, playedTimes: 0};
            
            logger.log('debug', 'Inserting ' + item + ' rom from db.');

            spinboxDb.insert("roms", filter, function(state) {
                if(state.ok == 0) {
                    error = 1;
                }
                
            });
            
            if(index == roms.length-1) {
                res.send({error: error});
            }
        });
    });
    
    app.post("/roms/deleteRoms", function(req, res, next) {
        var data = req.body; 
        var platform = data.platform;
        var roms = JSON.parse(data.roms);
        
        var error = 0;
        
        roms.forEach(function(item, index) {
            var name = item.split(".")[0];
            var filter = {file: item, platform: platform};
            
            logger.log('debug', 'Deleting ' + item + ' rom from db.');

            spinboxDb.delete("roms", filter, function(state) {
                if(state.ok == 0) {
                    error = 1;
                }               
            });
            
            if(index == roms.length-1) {
                res.send({error: error});
            }
        });
    });
    
    app.get("/moonlight/loadConfig", function(req, res, next) {   
        var filter = {active: 1};
        var fields = {"_id": false};
        
        spinboxDb.find("moonlight_config", filter, fields, "", 0, function(docs) {
            if(docs.length < 1) {
                docs = null;
            }
            res.send(docs[0]);
        });
    });
    
    app.get("/moonlight/getDevices", function(req, res, next) {   
        var fields = {"_id": false};
        
        spinboxDb.find("moonlight_config", null, fields, "", 0, function(docs) {
            if(docs.length < 1) {
                docs = null;
            }
            res.send(docs);
        });
    });
    
    app.post("/moonlight/saveConfig", function(req, res, next) {
        var update = req.body.data;
        var filter = {active: 1}
        
        spinboxDb.update("moonlight_config", filter, update, function(state) {
            var response = {error: 0};
            if(state.ok == 0) {
                response = {error: 1};
            }
            res.send(response);
        });
    });
    /*+++++++++++++++++*****************************************/
    
    

    /*+++++++++++++++++*****************************************/
    /*****    WIFI TOOLS   *************************************/
    /*+++++++++++++++++*****************************************/

    // Lista las redes WIFI que hay cerca con sus propiedades.
    app.get('/config/wifi/scan', function(req, res, next) {
        logger.log('debug', 'Scanning WIFI Acces Points.');
        wifi.scan(res);
    });

    // Conecta con la red wifi
    app.post('/config/wifi/connect', function(req, res, next) {
        logger.log('debug', 'Connecting to WIFI Acces Point.');
        var wifiProps = req.body;
        wifi.connectWifi(res, wifiProps);
    });

    // Devuelve la wifi a la que esta conectada
    app.get('/config/wifi/getWifi', function(req, res, next) {
        logger.log('debug', 'Getting wifi network name');
        wifi.getWifi(res);
    });
    /*+++++++++++++++++*****************************************/
    


    /*+++++++++++++++++*****************************************/
    /*****    AUDIO TOOLS     **********************************/
    /*+++++++++++++++++*****************************************/

    app.post('/config/audio/setVolumen', function(req, res, next) {
        var volume = req.body.volume;
        logger.log('debug', 'Configuring sound volume to ' + volume + '%');
        audio.setVolume(res, volume);

    });


    app.get('/config/audio/getVolumen', function(req, res, next) {
        logger.log('debug', 'Catching volume from system.');  
        audio.getVolume(res);
    });
    /*+++++++++++++++++*****************************************/
    
    
    /*+++++++++++++++++*****************************************/
    /***** MOONLIGHT TOOLS *************************************/
    /*+++++++++++++++++*****************************************/

    // Ejecuta el programa Moonlight desde un script que devuelve el codigo que se debe introducir.
    app.post("/moonlight/pair", function(req, res, next) {
        logger.log("info", "Pairing Moonlight with remote PC");
        //saveConfig('steam_config.json', req.body);

        var options = req.body;
        logger.log("info", "Pairing to .... " + options.remoteIP);
        var pCommand = 'python ' + __dirname + '/scripts/pair_moonlight.py ' + options.remoteIP;

        command.run(pCommand, function(stdout, stderr) {
            io.emit('pairingMoonlight', stderr);
        });

        res.send("OK");
    });

    // Recibe el codigo de emparejamiento desde el script lanzado encima.
    app.post("/python/pairCode", function(req, res, next) {
        logger.log("debug", "Received pairing code");
        var key = req.body.keyCode;
        logger.log("debug", "Key: " + key);

        io.emit('pairingCode', req.body);

        res.send("OK");

    });

    // Guarda la configuracion de steam
    app.post("/saveSteamConfig", function(req, res, next) {
        logger.log("debug", "Saving Steam configuration: " + req.body);
        logger.log("debug", req.body);
        saveConfig('steam_config.json', req.body);
    });

    // Carga la configuracion de steam
    app.get("/loadSteamConfig", function(req, res, next) {  
        logger.log("debug", "Loading Steam configuration.");
        var data = fs.readFileSync(__dirname + '/steam_config.json'),
            myObj;

        try {
            myObj = JSON.parse(data);
            logger.log("debug", myObj);
            logger.log("info", "Steam configuration loading succesfully.");
            res.send(JSON.stringify(myObj));
        }
        catch (err) {
            logger.log("error", "There is one error parsing JSON object.");
            logger.log("error", err);
        }
    });

    // Funcion para guardar un archivo de configuracion.
    var saveConfig = function(fileName, fileData) {
        var data = JSON.stringify(fileData);

        fs.writeFile(__dirname + '/' + fileName, data, function (err) {
            if (err) {
                logger.log("error", "There is one error saving Steam configuration");
                logger.log("error", err.message);
              return;
            }
            logger.log("debug", "Configuration saved succesfully");
        });  
    };

    // Funcion para lanzar el browser.
    var reloadApp = function() {
        logger.log("debug", "Initializing TV app.");

        command.run("sudo xinit /home/pi/startBrowser.sh");
    };
    /*+++++++++++++++++*****************************************/
    

}

module.exports = router;