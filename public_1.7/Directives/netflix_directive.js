var netflixApp = angular.module('netflix', []);

netflixApp.directive('netflixApp', function() {
    return {
        restrict: 'EA',
        templateUrl: 'Templates/netflix_template.html',
        controller: 'netflixCtrl'
    }
});