var steamApp = angular.module('RPIsteam', []);

steamApp.directive('steamApp', function() {
    return {
        restrict: 'EA',
        templateUrl: 'Templates/RPI_steam_template.html',
        controller: 'RPIsteamCtrl'
    }
});