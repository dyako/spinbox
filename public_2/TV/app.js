var app = angular.module("spinBox", ['ngRoute', 'ngResource', 'watch', 'ngAnimate', 'emulators', 'web', 'moonlight',
                                     'pulseButtonComponent', 'gravityButtonComponent', 'emulatorsInfoComponent', 'userMessageComponent']);
            
app.config(function($routeProvider, $locationProvider) {
    
    //$locationProvider.html5Mode(true);
    
    $routeProvider
    .when("/main", {
        templateUrl : "TV/Templates/main_menu.html",
        animation: 'fade'
    })
    .when("/apps", {
        templateUrl : "TV/Templates/apps_menu.html"
    })
    .when("/web", {
        templateUrl : "TV/Templates/web_menu.html"
    })
    .when("/emulators", {
        templateUrl : "TV/Templates/emulators_menu.html"
    })
    .when("/moonlight", {
        templateUrl : "TV/Templates/moonlight_menu.html"
    })
    .when("/roms/:emulator", {
        templateUrl : "TV/Templates/roms_menu.html"
    })
    .when("/config", {
        templateUrl : "TV/Templates/config_menu.htm"
    })
    .otherwise({
        templateUrl : "TV/Templates/main_menu.html",
    });
    
});
    
    
app.controller("indexCtrl", function($scope, $rootScope, $http, $location, keyEvents) {
    
    $scope.version = "2.0";
    $rootScope.ip = "";
    
    $rootScope.animations = true;
    
    $('body').css("overflow", "hidden");

    // Devuelve la Ip del servidor para mostrarla en pantalla.
    var getIp = function() {
        $http.get("/getIp").then(function (response) {
            console.log(response);
            $scope.ip= response.data.address[0] + ':8888';
        });
    }();

    
    
    $scope.getKeyEvent = function($event) {
        keyEvents.getKeyEvent($event);   
    };
    
    $scope.disableSelectedButton = function($event) {
        keyEvents.disableSelectedButton($event);  
    };
    
    
});