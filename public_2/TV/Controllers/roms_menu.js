emulators.controller("romsCtrl", function($scope, $routeParams, $http, $interval, $location, $userMessage) {
    
    $scope.emulator = $routeParams["emulator"];
    $scope.emulatorName = "Emulators / " + $scope.emulator;
    
    $scope.romsFolder = [];
    $scope.numRomsFolder = 0;
    $scope.numRomsDb = 0;
    $scope.roms = [];
    $scope.numRoms = 0;
    $scope.romsMessage = "";
    $scope.showRomsMessage = false;
    
    $scope.setLocation = function(path) {
        $location.path(path);
    };
  
    
    /******************/
    /* INIT ROMS LIST */
    /******************/
    
    var romsTemp = [];
    var intervalTime = 0;
    
    var getAllRoms = function() {
        $http.post("/db/getDocuments", {collection: 'roms', filter: {platform: $scope.emulator}}).then(function (response) {
            romsTemp = response.data;
            $scope.numRomsDb = romsTemp.length;
            intervalTime = 1400 / romsTemp.length;
            initRomsAnim();
            listRoms();
            removeInitAnim();
        });
    }();
    
    // Inicializa la animacion de las roms.
    var initRomsAnim = function() {
        var i = 0;
        var pushRoms = function() {
            $scope.roms.push(romsTemp[i]);
            i++;
            if(i>=romsTemp.length) { $interval.cancel(romsInterval) };
        };

        var romsInterval = $interval(pushRoms, intervalTime);
    };
    
    // Elimina la clase de la animacion en la inicializacion.
    var removeInitAnim = function() {
        $interval(function() {
            $(".roms-image-init-anim").removeClass("roms-image-init-anim");
        }, 1500, true);
    };
    /****************************************************************************************************/

    
    $scope.launchRom = function(rom) {
        $http.post('/retroarch/launchRom', { 'platform': $scope.emulator, 'rom': rom}).success(function(data) {   
            $scope.showUserMessage('Lanzando juego de ' + $scope.emulator + ': ' + rom, '', 10000);
        });
    };
    
    
    // Controla el mensaje
    var checkNumberRoms = function() {
        $scope.numRoms = $scope.numRomsFolder - $scope.numRomsDb;
        var romStr = "rom";
        if($scope.numRoms > 1 || $scope.numRoms < -1) { romStr = "roms" }
            
        if($scope.numRoms > 0) {
            $scope.romsMessage = "new " +romStr + " added";
        } else {
            $scope.romsMessage = romStr +" removed";
        }
        
        if($scope.numRoms != 0) {
            $scope.showRomsMessage = true;
        }
    };
    
    var listRoms = function() {
        $http.post('/retroarch/listRoms', { platform: $scope.emulator}).success(function(data) { 
            $scope.numRomsFolder = data.length;
            $scope.romsFolder = data;
            
            checkNumberRoms();
        });
    };
    
    
    
    /* ROMS UPDATE*/
    // Inserta las roms que detecta que no existen en la BBDD.
    var insertRoms = function(roms) {
        var jsonRoms = JSON.stringify(roms);

        $http.post("/roms/addRoms", {roms: jsonRoms, platform: $scope.emulator}).then(function(response) {
            if(response.data.error == 0) {
                roms.forEach(function(item, index) {
                    var name = item.split(".")[0];
                    var rom = {name: name, file: item, platform: $scope.platform, playedTimes: 0};
                    $scope.roms.push(rom);

                    if(index == roms.length-1) {
                        removeInitAnim();
                    }
                });
            } else {
                $userMessage.showMessage("One error updating roms on BD, please try again");
            }
        });
    };
    
    // Inserta las roms que detecta que no existen en la BBDD.
    var deleteRoms = function(roms, ids) {
        var jsonRoms = JSON.stringify(roms);
        $http.post("/roms/deleteRoms", {roms: jsonRoms, platform: $scope.emulator}).then(function(response) {
            if(response.data.error == 0) {
                var dec = 0;
                ids.forEach(function(item, index) {

                    $scope.roms.splice(item-dec, 1);
                    dec++;

                    if(index == ids.length-1) {
                        removeInitAnim();
                    }
                });
            } else {
                $userMessage.showMessage("One error updating roms on BD, please try again");
            }
        });
    };
    
    // Actualiza la lista de las roms. Comprueba cuales existen y cuales no en la carpeta del emulador.
    $scope.updateRoms = function() {
        var romsToInsert = [];
        var romsToDelete = [];
        var romsIdToDelete = [];
        
        // Comprueba las roms que hay que añadir a la BBDD
        $scope.romsFolder.forEach(function(item, index) {
            var i = 0;
            var exist = true;
            while(i < $scope.numRomsDb) {
                if(item == $scope.roms[i].file) { break; }
                if(i == $scope.roms.length-1) {
                    romsToInsert.push(item);
                }
                i++;
            }
            
            // Inserta las roms que detecta que no existen en la BBDD.
            if(index == $scope.numRomsFolder-1) {
                insertRoms(romsToInsert);
            }
        });
        
        
        //Comprueba las roms que hay que eliminar de la BBDD.
        $scope.roms.forEach(function(item, index) {
            var i = 0;
            var exist = true;
            while(i < $scope.numRomsFolder) {
                if(item.file == $scope.romsFolder[i]) { break; }
                if(i == $scope.romsFolder.length-1) {
                    romsToDelete.push(item.file);
                    romsIdToDelete.push(index);
                }
                i++;
            }
            
            // Inserta las roms que detecta que no existen en la BBDD.
            if(index == $scope.numRomsDb-1) {
                deleteRoms(romsToDelete, romsIdToDelete);
            }
        });
        
        $scope.showRomsMessage = false;
    };
    
    
    
    
});