app.controller("moonlightDevicesCtrl", function($scope, $http, $location) {
    
    
    $scope.moonlightDevices = [];

    // Carga los dispositivos que hay creados con todas sus propiedades.
    var getDevices = function() {
        $http.get("/moonlight/getDevices").then(function (response) {
            $scope.moonlightDevices = response.data;
            $scope.moonlightDevices.push(null);
        });
    }();
    
    
    // Lanza Moonlight en el dispositivo seleccionado.
    $scope.launchMoonlight = function(index) {
        console.log("boton lanzar moonlight " + index);
        //$scope.saveMoonlightConfig();
        $http.post("/moonlight/launchProgram", $scope.moonlightDevices[index]).success(function(data) {
            console.log("lanzar moonlight - " + data);
            //$scope.showUserMessage('Lanzando moonlight', '', 10000);
        });
    };
    
    
    // Redirige a la vista donde se configura un nuevo device.
    $scope.addDevice = function() {
        $location.path("/moonlight/config");
    };
    
});