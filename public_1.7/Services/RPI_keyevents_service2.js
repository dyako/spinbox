app.service('keyEvents', function () {


    var buttons = new Array();
    var length = 0;
    
    var xIndex = -1;
    var yIndex = -1;
    var enterPressed = false;
    
    // Metodo para añadir un boton al array donde se almacenan todos en su respectiva posicion.
    this.addButton = function(y, x, activeStyle, desactiveStyle, id, funcion) {
        var buttonElement = {y: y, x: x, activeStyle: activeStyle, desactiveStyle: desactiveStyle, id: id, function: funcion};
        
        buttons[y] = buttons[y]||[];
        buttons[y].push(buttonElement);
        
        if(length < y+1) {
            length = y+1;   
        }
        
        document.getElementById(buttons[y][x].id).className = buttons[y][x].desactiveStyle;

        //console.log("Se introduce objeto[" + y + '][' + x +']: ' + buttons[y][x].y + ' , ' + buttons[y][x].x + ' , ' + buttons[y][x].activeStyle + ' , ' + buttons[y][x].desactiveStyle + ' , ' + buttons[y][x].desactiveStyle + ' , ' + buttons[y][x].id);
    };
    
    
    // Metodo que gestiona los eventos de teclado.
    this.getKeyEvent = function($event) {
        //console.log("get Key Events");
        enterPressed = false;
        
        if(xIndex == -1 && yIndex == -1) {
            xIndex = 0;
            yIndex = 1;
        }
        else {
            // Mueve el cursor del array segun la tecla pulsada.
            if($event.keyCode == "37") { // FLECHA IZQUIERDA
                xIndex -= 1;
            }

            else if($event.keyCode == "39") { // FLECHA DERECHA
                xIndex += 1;
            }

            else if($event.keyCode == "38") { // FLECHA ARRIBA
                yIndex -= 1;
            }

            else if($event.keyCode == "40") { // FLECHA ABAJO
                yIndex += 1;
            }
            else if($event.keyCode == "13") { // BOTON SLECCIONAR , ENTER
                console.log("entra en seleccion de boton");
                if(buttons[yIndex][xIndex].function !== '') {
                    buttons[yIndex][xIndex].function();
                }
            }
        }
        
        this.checkLimits();
        
        this.setStyle();
        
    };
    
    // Metodo que comprueba los limites del array.
    this.checkLimits = function() {

        if(yIndex >= length) {
            yIndex = 0;
        }
        if(yIndex < 0) {
            yIndex = length-1;   
        }
        if(xIndex >= buttons[yIndex].length) {
            xIndex = 0;
        }
        if(xIndex < 0) {
            xIndex = buttons[yIndex].length - 1;   
        }

    };
    
    // Metodo que controla los estilos de los botones.
    this.setStyle = function() {
        for(var i=0; i<length; i++) {
            for(var j=0; j<buttons[i].length; j++) {
                document.getElementById(buttons[i][j].id).className = buttons[i][j].desactiveStyle;
                document.getElementById(buttons[i][j].id).blur();
            }
        }
        if(yIndex != -1 && xIndex != -1) {
            document.getElementById(buttons[yIndex][xIndex].id).className = buttons[yIndex][xIndex].activeStyle;
            document.getElementById(buttons[yIndex][xIndex].id).focus();
        }
    };
    
    
    // Metodo que desactiva la seleccion cuando se detecta un movimiento de raton.
    this.disableSelectedButton = function() {
        xIndex = -1;
        yIndex = -1;
        
        this.setStyle();
    };
    
    
    // Metodo que settea el cursor del array en una posicion.
    this.setCursorPosition = function(x, y) {
        xIndex = x;
        yIndex = y;
    };
    
    // Metodo para eliminar del array todos los botones de una linea.
    this.deleteRowButtons = function(row) {
        //console.log("tamaño antes: " + buttons[row].length);
        var len = buttons[row].length;
        buttons[row].splice(0, len);   
        
        length--;
        //console.log("tamaño despues: " + buttons[row].length);
    };
    
    // Metodo que elimina las filas desde una posicion hasta otra.
    this.deleteRowsToEnd = function (rowFrom) {
        var from = 0;
        
        if(rowFrom !== "" && rowFrom != 0) {
            from = rowFrom;
        }
        
        var tam = length;
        
        while(from<tam) {
            var len = buttons[from].length;
            buttons[from].splice(0, len);
            from++;
            length--;
        }
        
    };
    
    // Metodo para cambiar propiedades de un boton.
    this.changeButtonValues = function(y, x, activeStyle, desactiveStyle, id, funcion) {
        var button = buttons[y][x];
        
        if(activeStyle !== '') {
            button.activeStyle = activeStyle;   
        }
        if(desactiveStyle !== '') {
            button.desactiveStyle = desactiveStyle;   
        }
        if(id !== '') {
            button.id =  id;   
        }
        if(funcion !== '') {
            button.function = funcion;   
        }
    };
    
});