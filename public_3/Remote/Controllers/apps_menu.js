app.controller("appsCtrl", function($scope, $http, $interval) {

    var appsTemp = [];
    $scope.apps = [];
    
    // Coge las apps de la BBDD.
    var getApps = function() {
        $http.post("/db/getDocuments", {collection: "apps"}).then(function (response) {
            appsTemp = response.data; 
            initAppsAnim();
        });
    }();
    
    // Inicializa la animacion de las apps.
    var initAppsAnim = function() {
        var i = 0;
        var pushapps = function() {
            $scope.apps.push(appsTemp[i]);
            i++;
            if(i>=appsTemp.length) { $interval.cancel(appsInterval) };
        };

        var appsInterval = $interval(pushapps, 200);
    };
    
    
    // Lanza la aplicacion correspondiente.
    $scope.setAction = function(action) {
        if(action == "/kodi") {
            $http.post("/kodi").then(function(res) {
            });
        }
        
        if(action == "/retropie") {
            
        }
    };
   
});