var userMessage = angular.module("userMessageComponent", []);

userMessage.directive("userMessage", function() {
   
    return {
        restrict: 'E',
        template:   ' <style>' +
                    ' .user-message { position: absolute; background: #5d486a; color: white; top: 90vh; width: 50vw; font-size: 1.2vw; padding: 1vw; padding-left: 3vw; padding-right: 0.7vw; opacity: 0;' +
                    ' animation: slideRight 10s ease-in-out;} ' +
                    ' @keyframes slideRight { 0% { opacity: 1; left: -50vw; } ' +
                                          '  5% { opacity: 1; left: 0vw;  }' +
                                          '  95% { opacity: 0.8; left: 0vw;  }' +
                                          '  100% {opacity: 0; left: 0vw;  } }' +
                    ' </style>' +
                    ' <div id="user-mess" class="user-message" hidden></div>',
    }
    
});



userMessage.service("$userMessage", function() {
    
    
    var destroyMessageDiv = function() {

        $('#user-mess').text("");
        $('#user-mess').removeClass("user-message");
        $('#user-mess').hide();
    };
    
    var createMessageDiv = function(message) {

        destroyMessageDiv();
        
        $('#user-mess').show();
        $('#user-mess').addClass("user-message");
        $('#user-mess').text(message);

    };
    
        
    // METODOS DEL SERVICIO
    return {
        
        showMessage : function(message) {
            createMessageDiv(message);
        },
        
        hideMessage : function() {
            destroyMessageDiv();
        }
        
    }
});