// Modulos de dependencias
var express     = require('express');
var app         = express();
var http        = require('http').Server(app);
var io          = require('socket.io')(http);

var path        = require('path');
var bodyParser  = require('body-parser');

var robot       = require('./modules/Robotjs.js');
var logger      = require('./modules/Logger.js');
var Router      = require('./router.js');


//Configuracion del servidor
app.set('port', 8888);
app.set('appName', 'Spin Box');
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use("/TV",express.static(__dirname + '/public_3'));
app.use("/Remote",express.static(__dirname + '/public_3'));
app.use("/TV-2.0",express.static(__dirname + '/public_2'));
app.use("/TV-1.7",express.static(__dirname + '/public_1.7'));


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


var router = Router(app, io);


// Crea el servidor en el puerto predeterminado.
http.listen(app.get('port'));

logger.log("info", "Server beggining on port: " + app.get('port'));



function initEnv() {
    try {
        process.env.DISPLAY = ':0.0';
        process.env.SUDO_GID = '1000';
        process.env.SUDO_UID = '1000';
        process.env.USER = 'root';
        process.env.HOME = '/root';
        process.env.LOGNAME = 'root';
        process.env.TERM = 'xterm';
        process.env.USERNAME = 'root';
        process.env.SUDO_USER = 'pi';
        process.env.PWD = '/home/pi';
        process.env.SUDO_COMMAND = '/etc/rc.local';
        process.env.SHELL = '/bin/bash';
        logger.log("debug", "------- ENVIROMENTAL VARS ---------");
        logger.log("debug", process.env);
    } catch (err) {
        logger.log('error', 'Error trying to change user permissions.');
        process.exit(1);
    }
};

initEnv();