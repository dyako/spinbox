var app = angular.module('remoteSpinBox', ['ngRoute']);

app.config(function($routeProvider, $locationProvider) {
    
    //$locationProvider.html5Mode(true);
    
    $routeProvider
    .when("/main", {
        templateUrl : "Templates/remote_menu.html",
        animation: 'fade'
    })
    .when("/apps", {
        templateUrl : "Templates/apps_menu.html"
    })
    .when("/web", {
        templateUrl : "Templates/web_menu.html"
    })
    .when("/emulators", {
        templateUrl : "Templates/emulators_menu.html"
    })
    .when("/moonlight", {
        templateUrl : "Templates/moonlight_menu.html"
    })
    .otherwise({
        templateUrl : "Templates/remote_menu.html",
    });
    
});


app.controller("indexCtrl", function($scope, $rootScope, $http, $location) {
    
});