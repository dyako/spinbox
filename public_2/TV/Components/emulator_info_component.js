var emulatorInfo = angular.module('emulatorsInfoComponent', []);

emulatorInfo.directive('emulatorsInfo', function() {
    return {
        restric: 'E',
        template:   " <style> " +
                    " .container-description { z-index: 50; padding-top: 4.6vh; padding-left: 2vw; width: {{width}}vw; float: left; } " +
                    " .div-description { z-index: 50; background: #cccccc; height: {{height}}vh; width: {{width}}vw; box-shadow: 10px 10px 5px #333333; } " +
                    " .image-description { width: 100%; height: 42%; } " +
                    " .name-description { font-size: 2.2em; padding: 0.5vw; padding-left: 1vw; font-weight: bold; background: #dddddd; height: 10%; } " +
                    " .top-name-description { font-size: 1.5em; padding: 0.5vw; padding-left: 1vw; height: 8%; background: #cccccc } " +
                    " .top-played-description { font-size: 1.5em; padding: 0.5vw; padding-left: 1vw; height: 8%; background: #aaaaaa } " +
                    " </style> " +
        
                    " <div class='container-fluid container-description'> "+
                    " <div class='div-description'> " +
                    " <img class='image-description' src='{{src}}'> " +
                    " <div class='name-description'>{{name}}</div> " +
                    " <div class='top-name-description'>Top played</div> " +
                    " <div class='top-played-description' ng-repeat='top in topPlayed'> " +
                    " <div class='top-played' >{{top.name}}</div> " +
                    " </div> " +
                    " </div> </div> ",
        
                            
        controller: "emulatorInfoCtrl",
        scope: { 
                    width: "@width",
                    height: "@height",
                    src: "=",
                    name: "=",
                    top: "="
               }
    };

});


emulatorInfo.controller('emulatorInfoCtrl', function($scope, $interval) {

    $scope.topPlayed = [];
    
    // Juegos mas jugados de esta plataforma.
    //$scope.topPlayed = [{name: '1. Super Mario All stars'}, {name: '2. Mario Kart'}, {name: '3. Street Figther'}, {name: '4. Earth Worm Jim'}, {name: '5. Metroid Prime'}];
    $(".container-description").ready(function() {
        
        var initTopRoms = function() {

            for(var i=0; i<5; i++) {
                if($scope.top != null && $scope.top.length-1 >= i) {
                    $scope.topPlayed.push({name: (i+1 + ". " + $scope.top[i].name)});
                } else {
                    $scope.topPlayed.push({name: ""});
                }
            }
        }();
        
    });

});