var logger  = require('./Logger.js');
var Command = require('./Command.js');

var command = new Command();

function Wifi() {

    var interface = "wlan0";
    var scanCmd = "sudo iwlist wlan0 scan";
};

// Metodo que devuelve todas las redes wifis que hay en la zona
Wifi.scan = function (res) {
    logger.log("info", "scaneando");
    logger.log("debug", "sudo iwlist wlan0 scan");
    command.run("sudo iwlist wlan0 scan", function(stdout) {
         // Separa las distintas redes wifi en un array.
        var list = stdout.split("Cell");
        var wifi_array = [];
        for (var i = 0; i < list.length; i++) {
            wifi_array[i] = list[i].split("\n");
        }

        var wifi_list = [];

        // Carga las propiedades de todas las redes WIFI que detecta la tarjeta de red.
        for (var i = 0; i < wifi_array.length; i++) {
            var essid = "";
            var bssid = "";
            var channel = "";
            var enc = "";
            var power = "";

            for (var j = 0; j < wifi_array[i].length; j++) {
                if (wifi_array[i][j].indexOf("ESSID:") > -1) {
                    var pos = wifi_array[i][j].indexOf("ESSID:");
                    essid = wifi_array[i][j].substr(pos + 7).split('"', 1)[0];
                }
                
                if (wifi_array[i][j].indexOf("Address:") > -1) {
                    var pos = wifi_array[i][j].indexOf("Address:");
                    bssid = wifi_array[i][j].substr(pos + 9);
                }
                
                if (wifi_array[i][j].indexOf("Channel:") > -1) {
                    var pos = wifi_array[i][j].indexOf("Channel:");
                    channel = wifi_array[i][j].substr(pos + 8);
                }
                
                if (wifi_array[i][j].indexOf("level=") > -1) {
                    var pos = wifi_array[i][j].indexOf("level=");
                    power = wifi_array[i][j].substr(pos + 6);
                }
                
                if (wifi_array[i][j].indexOf("WPA") > -1) {
                    enc = 'WPA';
                }

            }
            
            if(wifi_array[i].length > 2) {
                wifi_list.push({
                    "essid" : essid,
                    "bssid" : bssid,
                    "channel" : channel,
                    "power" : power,
                    "enc" : enc,
                    "passwd": ''
                });
            }
        }
    });
    
    command.run("sudo iwlist wlan0 scan", function() {
        // Si las redes ya tienen alguna contraseña guardada la carga.
        command.run("cat /etc/wpa_supplicant/wpa_supplicant.conf", function(stdout) {
            var wpa_supplicant = stdout.split("\n");

            var wpa_passwd = [];
            var ssid = "";
            var psk = "";

            // Parsea todas las passwords almacenadas en el archivo de configuracion wpa_supplicant.conf y las almacena en wpa_supplicant[].
            for (var j=0; j<wpa_supplicant.length; j++) {
               if (wpa_supplicant[j].indexOf("ssid=") > -1) {
                    var pos = wpa_supplicant[j].indexOf("ssid=");
                    ssid = wpa_supplicant[j].substr(pos + 6).split('"', 1)[0];
                }

                if (wpa_supplicant[j].indexOf("psk=") > -1) {
                    var pos = wpa_supplicant[j].indexOf("psk=");
                    psk = wpa_supplicant[j].substr(pos + 5).split('"', 1)[0];
                }

                if (wpa_supplicant[j].indexOf("}") > -1) {

                    wpa_passwd.push({
                        "ssid" : ssid,
                        "psk" : psk
                    });

                    logger.log("debug", wpa_passwd);    

                    ssid = "";
                    psk = "";
                }                                   
            }


            // Rellena las passwd en las redes correspondientes.
            for(var j=0; j<wpa_passwd.length; j++) {
                for(var i=0; i<wifi_list.length; i++) {

                    if(wpa_passwd[j].ssid == wifi_list[i].essid) {
                        wifi_list[i].passwd = wpa_passwd[j].psk;
                        logger.log("debug", wifi_list[i]);
                    }
                }
            }
        });
    });

    res.send(JSON.stringify(wifi_list));
};


// Metodo que devuelve las propiedades de la red que esta conectada.
Wifi.getWifi = function(res) {
    logger.log("info", "Getting Wifi properties");
    
    var bssid = "";
    var essid = "";
    var freq = "";
    var signal = "";
    var ip = "";
    
    // Carga las propiedades de la red a la que esta conectada el dispositivo.
    command.run("iw wlan0 link", function(stdout) {
        var wifi = stdout.split('\n');
        
        for(var i=0; i<wifi.length; i++) {
            
           if (wifi[i].indexOf("Connected to") > -1) {
                var pos = wifi[i].indexOf("Connected to");
                bssid = wifi[i].substr(pos + 13, pos+13+18);
            }
            
            if (wifi[i].indexOf("SSID:") > -1) {
                    var pos = wifi[i].indexOf("SSID:");
                    essid = wifi[i].substr(pos + 6);
            }
            
            if (wifi[i].indexOf("freq:") > -1) {
                    var pos = wifi[i].indexOf("freq:");
                    freq = wifi[i].substr(pos + 6);
            }
            
            if (wifi[i].indexOf("signal:") > -1) {
                    var pos = wifi[i].indexOf("signal:");
                    signal = wifi[i].substr(pos + 7);
            }
        }
        
        // Carga la ip de la red a la que esta conectado.
        command.run("ifconfig wlan0", function() {
            wifi = stdout.split('\n');
            
            for(var i=0; i<wifi.length; i++) {
                
                if (wifi[i].indexOf("inet addr:") > -1) {
                    var posBegin = wifi[i].indexOf("inet addr:");
                    var posEnd = wifi[i].indexOf("Bcast:");
                    ip = wifi[i].substr(posBegin + 10, posEnd - (posBegin+10) - 1);
                }
            }
            
             var wifi_props = {
                    "essid" : essid,
                    "bssid" : bssid,
                    "freq" : freq,
                    "signal" : signal,
                    "ip": ip.trim()
                };
        
            logger.log("debug", wifi_props);

            res.send(JSON.stringify(wifi_props));
        });
    });
    
};


// Metodo que modifica archivos de sistema para conectarse a una red wifi.
Wifi.connectWifi = function(res, wifiProps) {
    logger.log("info", "Connecting with  WIFI accex point..");
    
    
    // Comprueba si la red ya esta guardada y si es asi la elimina.
    command.run("sudo cat /etc/wpa_supplicant/wpa_supplicant.conf", function(stdout) {
        var wpa_supplicant = stdout.split('\n');
        
        var posBegin    = 0;
        var posEnd      = 0;
        var elim        = false;
        
        for (var j=0; j<wpa_supplicant.length; j++) {
           if (wpa_supplicant[j].indexOf("network") > -1) {
                posBegin = j;
            }

            if (wpa_supplicant[j].indexOf(wifiProps.essid) > -1) {
                elim = true;
            }

            if (wpa_supplicant[j].indexOf("}") > -1) {
                posEnd = j;
                if(elim) break;
            }                                   
        }
        
        if(elim) {
            wpa_supplicant.splice(posBegin, posEnd-posBegin+2);
        }
        
        // Crea la plantilla 
        var wifiRemoved = '';
        for(var i=0; i<wpa_supplicant.length; i++) {
            if(wifiRemoved[i] != '') {
                wifiRemoved += wpa_supplicant[i] + '\n';
            }
        }
        
        command.run("sudo printf '" + wifiRemoved + "' > /etc/wpa_supplicant/wpa_supplicant.conf", function() {
            // Crea la plantilla 
            var wifiTmp =  '';//'\n# Configuracion de conexion wifi ' + wifiProps.essid + '\n';
            wifiTmp += 'network={\n\t';
            wifiTmp += 'ssid="' + wifiProps.essid + '"\n\t';
            wifiTmp += 'psk="' + wifiProps.passwd + '"\n\t';
            wifiTmp += 'key_mgmt=WPA-PSK\n';
            wifiTmp += '}\n';

            logger.log("debug", wifiTmp);
            
            command.run("sudo printf '" + wifiTmp + "' >> /etc/wpa_supplicant/wpa_supplicant.conf", function() {
                command.run("sudo ifdown wlan0", function() {
                    setTimeout(function() {
                        command.run("sudo ifup wlan0", function() {
                            res.send(JSON.stringify(wifiProps));     
                        });
                    }, 5000);
                });
            });
        });
    });
};



module.exports = Wifi;