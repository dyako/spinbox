 You have to edit this file /etc/xdg/lxsession/LXDE-pi/autostart

@lxpanel --profile LXDE
@pcmanfm --desktop --profile LXDE

@xset s off
@xset -dpms
@xset s noblank
@sed -i 's/"exited_cleanly": false/"exited_cleanly": true/' ~/.config/chromium/Default/Preferences
@chromium --noerrdialogs --disable-translate --kiosk http://www.google.com?

----------------------------------------------------------------------------------

Raspberry PI kiosk mode with Chromium.
Posted: 09/16/2012 | Author: lokir | Filed under: Other, raspberry pi | Tags: Chromium, Raspberry Pi |50 Comments
1. Install chromium, x11-xserver-utils and unclutter

sudo apt-get update && apt-get upgrade -y
sudo apt-get install chromium x11-xserver-utils unclutter

2. We need to prevent screen from going blank and disable screen saver.
� edit /etc/xdg/lxsession/LXDE/autostart and comment # screen saver line and add those lines:

@xset s off
@xset -dpms
@xset s noblank
@chromium --kiosk --incognito http://some.web.