retropieApp.controller('retropieCtrl', function($scope, $http) {
    
    $scope.hideOptions = true;
    
    $scope.switchHideOptions = function()
    {
        if($scope.hideOptions) {
            $scope.hideOptions = false;   
        }
        else {
            $scope.hideOptions = true;   
        }
    }

    $scope.launchRetropie = function() {
        console.log("boton lanzar retropie");
        $http.post("/retropie").success(function(data) {
            console.log("lanzar retropie - " + data);
        });
    };
    
});