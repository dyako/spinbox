var logger  = require('./Logger.js');
var Command = require('./Command.js');

var command = new Command();

function Audio() {

    var volumen = "0";
};


Audio.setVolume = function(res, volume) {
    
    command.run("amixer sset 'PCM' " + volume + '%', function() {
        res.send('OK');
    });
};


Audio.getVolume = function(res, volume) {
    
    command.run("amixer get 'PCM' " + volume, function(stdout) {
        // Procesa la salida del comando para sacar el volumen del sistema.
          
          var amixer_array = [];
          
          amixer_array = stdout.split('\n');
          
          for(var i=0; i<amixer_array.length; i++) {
           
              if(amixer_array[i].indexOf('[') > -1) {
                  var pos_begin = amixer_array[i].indexOf('[');
                  var pos_end   = amixer_array[i].indexOf('%');
                  
                  this.volume = amixer_array[i].substr(pos_begin+1, pos_end - pos_begin-1);
                  
                  logger.log("debug", "System volume: " + this.volume);
                  res.send(this.volume);
              }
          }
    });
};


module.exports = Audio;