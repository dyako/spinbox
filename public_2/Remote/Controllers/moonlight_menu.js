app.controller("moonlightMenuCtrl", function($scope, $http) {
   
    $scope.moonlightOptions = null;

    var loadConfig = function() {
        $http.get("/moonlight/loadConfig").then(function (response) {
            console.log(response.data);
            $scope.moonlightOptions = response.data;
            console.log('Configuracion cargada: ' + JSON.stringify($scope.moonlightOptions));
        
            if($scope.moonlightOptions == null) {
                $scope.moonlightOptions = {
                        remoteIP: '0.0.0.0',
                        resolution: '1080',
                        fps: '60',
                        paired: false
                };
            };
            
            $scope.setActive('resolution', $scope.moonlightOptions.resolution+'Radio');
            $scope.setActive('fps', $scope.moonlightOptions.fps+'fpsRadio');
            
        });
    }();
    
    
    
    /* CONTROL RADIO BUTTONS */
    // Settea el varlo de los radio buttons en las variables.
    var setValue = function(value) {
        if(value == "1080Radio") {
            $scope.moonlightOptions.resolution = "1080";
        }
        if(value == "720Radio") {
            $scope.moonlightOptions.resolution = "720";
        }
        if(value == "30fpsRadio") {
            $scope.moonlightOptions.fps = "30";
        }
        if(value == "60fpsRadio") {
            $scope.moonlightOptions.fps = "60";
        }
    };
    
    // Funcion para activar los radio buttons.
    $scope.setActive = function(name, val) {
         var elems = $('[name='+name+']');
         $.each(elems, function(key, value) {
             if(value.id == val) {
                 value.className = "moonlight-radio-button-selected";
                 setValue(val);
             } else {
                 value.className = "moonlight-radio-button";
             }
         });  
     };
    
    
    
    
    $scope.launchMoonlight = function() {
        console.log("boton lanzar moonlight");
        $scope.saveMoonlightConfig();
        $http.post("/moonlight/launchProgram", $scope.moonlightOptions).success(function(data) {
            console.log("lanzar moonlight - " + data);
            $scope.showUserMessage('Lanzando moonlight', '', 10000);
        });
    };
                                                          
    
    $scope.pairMoonlight = function() {
        console.log("boton de pair");
        $scope.saveMoonlightConfig();
        $http.post("/moonlight/pair", $scope.moonlightOptions).success(function(data) {
            console.log("lanzar pair - " + data);
        });
    };


    $scope.saveMoonlightConfig = function() {
        console.log("Guardando configuracion " + JSON.stringify($scope.moonlightOptions));
        $http.post("/moonlight/saveConfig", {data: $scope.moonlightOptions}).then(function(response) {
            var error = JSON.stringify(response.data.error);
            if(error == 1) {
                $userMessage.showMessage("Error Saving moonlight options on DB, try again later.");
            }
        });
    };
    
});