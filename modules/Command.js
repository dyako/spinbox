var exec    = require('child_process').exec;
var logger  = require('./Logger.js');

function Command() {};

Command.prototype.run = function(command, callback) {
    exec(command, function(error, stdout, stderr) {
        if(stdout !== "") { logger.log('debug', 'stdout: ' + stdout); }
        if(stderr !== "") { logger.log('error', 'stderr: ' + stderr); }
        if (error !== null) {
            logger.log('error', 'exec error: ' + error);
        }
        callback(stdout, stderr, error);
    });
};

module.exports = Command;