var retropieApp = angular.module('retropie', []);

retropieApp.directive('retropieApp', function() {
    return {
        restrict: 'EA',
        templateUrl: 'Templates/retropie_template.html',
        controller: 'retropieCtrl'
    }
});