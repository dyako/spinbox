var fs          = require('fs');

var logger  = require('./Logger.js');
var Command = require('./Command.js');

var command = new Command();

var retroarchRomsPath = '/home/pi/RetroPie/roms/';

function Retroarch() {
    
};

Retroarch.prototype.listRoms = function(platform) {
    logger.log("debug", "Listing files of platform: " + platform);
    
    if(platform == 'mame') {
        platform = 'mame-mame4all';
    }
    
    var roms = [];
    
    fs.readdir(retroarchRomsPath + platform, function(err, items) {
        
        if(items == undefined) {
            return null;
        }
        
        for(var i=0; i<items.length; i++) {
            if(platform == 'snes' && items[i].indexOf('.smc') > -1) {
                roms.push(items[i]);
            }
            
            if(platform == 'gba' && items[i].indexOf('.gba') > -1) {
                roms.push(items[i]);
            }
            
            if(platform == 'neogeo' && items[i].indexOf('.zip') > -1) {
                roms.push(items[i]);
            }
            
            if(platform == 'mame-mame4all' && items[i].indexOf('.zip') > -1) {
                roms.push(items[i]);
            }
            
            if(platform == 'nes' && items[i].indexOf('.nes') > -1) {
                roms.push(items[i]);
            }
            
            if(platform == 'n64' && (items[i].indexOf('.z64') > -1 || items[i].indexOf('.v64') > -1)) {
                roms.push(items[i]);
            }
            
            if(platform == 'psx' && items[i].indexOf('.bin') > -1 ) {
                roms.push(items[i]);
            }
        }
        
        return roms;
    });
};


Retroarch.prototype.launchRom = function(platform, rom, config, callback) {
    logger.log("info", "Launching game of " + platform + ': ' + rom);
    
    logger.log("debug", "Retroarch Configuration: " + config);
    
    var lib = "";
    //var config = "";
    if(platform == 'snes') {
        lib = "-L /opt/retropie/libretrocores/lr-snes9x-next/snes9x_next_libretro.so ";
        //config = "--appendconfig /opt/retropie/configs/snes/retroarch.cfg ";
    }
    
    if(platform == 'gba') {
        //lib = "-L /opt/retropie/libretrocores/lr-gpsp/gpsp_libretro.so ";
        lib = "-L /opt/retropie/libretrocores/lr-mgba/mgba_libretro.so ";
    }
    
    if(platform == 'neogeo') {
        lib = "-L /opt/retropie/libretrocores/lr-fba/fb_alpha_libretro.so ";
    }
    
    if(platform == 'mame') {
        lib = "-L /opt/retropie/libretrocores/lr-imame4all/mame2000_libretro.so ";
    }
    
    if(platform == 'nes') {
        lib = "-L /opt/retropie/libretrocores/lr-nestopia/nestopia_libretro.so ";
    }
    
    if(platform == 'n64') {
        lib = "-L /opt/retropie/libretrocores/lr-mupen64plus/mupen64plus_libretro.so ";
    }
    
    if(platform == 'psx') {
        lib = "-L /opt/retropie/libretrocores/lr-pcsx-rearmed/libretro.so ";
    }
    
    var rCommand = '/opt/retropie/emulators/retroarch/bin/retroarch ' + lib + config + '"/home/pi/RetroPie/roms/' + platform + '/' + rom + '"';
    logger.log("debug", "Command: " + rCommand);

    command.run(rCommand, function() {
        callback();
    });
};


module.exports = new Retroarch();