#!/bin/sh
xset -dpms     # disable DPMS (Energy Star) features.
xset s off       # disable screen saver
xset s noblank # don't blank the video device
unclutter &
matchbox-window-manager &

# CHROMIUM
#sed –i ‘s/”exited_cleanly”: false/”exited_cleanly”: true/’~/.config/chromium/Default/Preferences
#chromium-browser –noerrdialogs –disable-translate –kiosk http://www.google.com

# EPIPHANY
#epiphany-browser -a --profile ~/.config http://127.0.0.1:8888/TV

#MIDORI
midori -e Fullscreen -a http://127.0.0.1:8888/TV