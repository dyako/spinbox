mouseControlApp.controller('mouseCtrl', function($scope, socket) {

    // Inicializa el tamaño del div que controla el raton.
    $scope.initMouseControl = function() {

        var width = window.innerWidth;
        var height = window.innerHeight;

        console.log('width: ' + width);
        console.log('height: ' + height);

        var moveMouse   = document.getElementById('move-mouse');
        var scrollMouse = document.getElementById('scroll-mouse');
        
        moveMouse.style.height    = (height - 250)*0.8 + "px";
        scrollMouse.style.height  = (height - 250)*0.8 + "px";
        /*
        moveMouse.style.minHeight    = "500px";
        scrollMouse.style.minHeight  = "500px";
        
        moveMouse.style.maxHeight    = "500px";
        scrollMouse.style.maxHeight  = "500px";
        */
        moveMouse.style.marginTop   = (height - 250)*0.2 + "px";
        scrollMouse.style.marginTop = (height - 250)*0.2 + "px";

    };
    
    /************************************/
    /* CONTROLA EL MOVIMIENTO DEL RATON */
    /************************************/
    var drag = false;
    var mouseDown = false;
    var mouseX = 0;
    var mouseY = 0;
    
    $scope.onMouseUp = function() {
        //console.log("on MOUSE UP event");
        
        if(!drag && mouseDown) {
            console.log('click mouse');
            socket.emit('clickMouse', "", function() {
                // CALLBACK   
            });   
        }
        
        drag = false;
        mouseDown = false;
        mouseX = 0;
        mouseY = 0;
    };
    
    $scope.onMouseDown = function(event) {
        //console.log("on MOUSE DOWN event");
        
        mouseX = event.clientX;
        mouseY = event.clientY;
        mouseDown = true;
        
        console.log('mouse DOWN: ' + mouseX + ', ' + mouseY);
    };
    
    
    $scope.onMouseDrag = function(event) {
        console.log("on DRAGG event");

        // Comienza el desplazamiento si hay un offset mayor que 50px, para diferenciar bien de un click 
        if( (!drag) && (mouseDown) && (Math.abs(mouseX - event.clientX) > 50 || Math.abs(mouseY - event.clientY) > 50)) {
            drag = true;
            mouseX = event.clientX;
            mouseY = event.clientY;
        }
        
        if((drag) && (mouseDown) && (Math.abs(mouseX - event.clientX) > 20 || Math.abs(mouseY - event.clientY) > 20)) {
            // envia el movimiento del  raton.
            console.log('mouse MOVE: ' + event.clientX + ', ' + event.clientY);
            var offsetX = -(mouseX - event.clientX);
            var offsetY = mouseY - event.clientY;
            var data = {x : offsetX, y : offsetY};
            mouseX = event.clientX;
            mouseY = event.clientY;
            
            socket.emit('moveMouse', data, function() {
                 // CALLBACK
            });
        }

    };

    /********************************/
    /* CONTROLA EL SCROLL DEL RATON */
    /********************************/
    
    var scrollDrag = false;
    
    $scope.onScrollUp = function() {
        //console.log("on MOUSE UP event");
        
        scrollDrag = false;
        mouseDown = false;
        mouseY = 0;
    };
    
    // Controla el desplazamiento del div lateral que controla el scroll de una pagina
    $scope.onMouseScroll = function(event) {
        //console.log("on DRAGG event");

        if( (!scrollDrag) && (mouseDown) && Math.abs(mouseY - event.clientY) > 50) {
            scrollDrag = true;
            mouseY = event.clientY;
        }
        
        if((scrollDrag) && (mouseDown) && Math.abs(mouseY - event.clientY) > 20) {
            // envia el movimiento de scroll.
            console.log('mouse SCROLL: ' + event.clientY);
            var direction = ((mouseY - event.clientY) > 0) ? 'up':'down';
            var offsetY = Math.abs(mouseY - event.clientY);
            var data = {magnitude: offsetY, direction: direction};
            socket.emit('scrollMouse', data, function() {
                 // CALLBACK
            });
            mouseDown = false;
        }

    };
    

    /***********************/
    /* CONTROLA EL TECLADO */
    /***********************/
    $scope.text2Send = "";
    $scope.sendText = function() {
        console.log('Enviando texto: ' + $scope.text2Send);
        socket.emit('sendText', {text: $scope.text2Send}, function() {
            // CALLBACK   
        });
    };
    
    $scope.sendButton = function(button) {
        console.log('Enviando boton: ' + button );
        socket.emit('sendButton', {button : button}, function() {
            // CALLBACK   
        });
    };
    

});