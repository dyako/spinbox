app.controller('mainMenuCtrl', function($scope, $http, $rootScope, keyEvents, $interval, $location, $userMessage) {
    
    // Opciones del menu principal.
    $scope.optionsTemp = [];
    
    // Recupera las opciones del menu principal.
    var getMainOptions = function() {
        $http.post("/db/getDocuments", {collection: "menu_options"}).then(function (response) {
            $scope.optionsTemp = response.data;
            
            $scope.optionsTemp.forEach(function(item, index) {
                $http.post("/db/getDocuments", {collection: item.name.toLowerCase(), filter: {"top": 1}}).then(function (response) {
                    item.top = response.data;
                    
                    // Rellena de nulls el array hasta un tamaño de 3.
                    var l = item.top.length;
                    while(l < 3) {
                        item.top.push({id: ''});
                        l++;
                    }
                });
            });
            
            initMainOptionsAnim();
        });
    }();
    
    $scope.options = [];
    
    
    // Funcion que se encarga de configurar los botones del main menu.
    var setMainMenuButtons = function() {
        keyEvents.addButton(0, 0, 'main-option-div apps-background apps-focus', 'main-option-div apps-background', 'apps-id', '');
        keyEvents.addButton(0, 1, 'main-option-div emulators-background emulators-focus', 'main-option-div emulators-background', 'emulators-id', '');
        keyEvents.addButton(0, 2, 'main-option-div moonlight-background moonlight-focus', 'main-option-div moonlight-background', 'moonlight-id', '');
        keyEvents.addButton(0, 3, 'main-option-div web-background web-focus', 'main-option-div web-background', 'web-id', '');
    }();
    
    
    
    // Inicializa la animacion de los main options.
    var initMainOptionsAnim = function() {
        var i = 0;
        var pushMainOptions = function() {
            $scope.options.push($scope.optionsTemp[i]);
            i++;
            if(i>=4) { $interval.cancel(optionsInterval) };
        };

        var optionsInterval = $interval(pushMainOptions, 400);
    };
    
    $scope.setLocation = function(path) {
        $location.path(path);
    };
    
    $scope.switchToVersion = function() {
        location.href = "/TV-1.7";
    };
    
    
    
    /**********************/
    /* BOTONES SUPERIORES */
    /**********************/
    
    // Funciones para hacer aparecer/desaparecer los nombres de los botones superiores.
    $scope.showButtonName = function(elemId) {
        $('#'+elemId).animate({opacity: "1"}, 200);
    };
    
    $scope.hideButtonName = function(elemId) {
        $('#'+elemId).animate({opacity: "0"}, 200);
    };
    
    
    // Funciones para controlar las funcionalidades de los botones superiores.
    $scope.turnoffSystem = function() {
        $http.get("/powerOff").then(function(response) {
            var error = response.data.error;
            console.log("error: " + error);
            if(error == 1) {
                $userMessage.showMessage("Error shuting down system, please try again");
            }
        });
    };
    
    $scope.restartSystem = function() {
        $http.get("/restartSystem").then(function(response) {
            var error = response.data.error;
            console.log("error: " + error);
            if(error == 1) {
                $userMessage.showMessage("Error restarting system, please try again");
            }
        });
    };
}); 