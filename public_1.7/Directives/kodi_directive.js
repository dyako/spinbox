var kodiApp = angular.module('kodi', []);

kodiApp.directive('kodiApp', function() {
    return {
        restrict: 'EA',
        templateUrl: 'Templates/kodi_template.html',
        controller: 'kodiCtrl'
    }
});