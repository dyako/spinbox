var app = angular.module('remoteSpinBox', ['ngRoute']);

app.config(function($routeProvider, $locationProvider) {
    
    //$locationProvider.html5Mode(true);
    
    $routeProvider
    .when("/main", {
        templateUrl : "Remote/Templates/remote_menu.html",
        animation: 'fade'
    })
    .when("/apps", {
        templateUrl : "Remote/Templates/apps_menu.html"
    })
    .when("/web", {
        templateUrl : "Remote/Templates/web_menu.html"
    })
    .when("/emulators", {
        templateUrl : "Remote/Templates/emulators_menu.html"
    })
    .when("/moonlight", {
        templateUrl : "Remote/Templates/moonlight_devices.html"
    })
    .when("/moonlight/config", {
        templateUrl : "Remote/Templates/moonlight_config.html"
    })
    .otherwise({
        templateUrl : "Remote/Templates/remote_menu.html",
    });
    
});


app.controller("indexCtrl", function($scope, $rootScope, $http, $location) {
    
});