var app = angular.module("mediaCenterJS", ['RPIsteam', 'watch', 'configMenu', 'gamepadConfig']);
            
app.controller("bodyCtrl", function($scope, $rootScope, $http, $timeout, socket, keyEvents) {
    
    $scope.version = "1.7";
    
    /**************************************/
    /* VARIABLES DE CONTROL DE LAS VISTAS */
    /**************************************/
    
    $scope.showAppsDiv = true;                  // variable que controla si se muestra el container de las apps.
    $scope.showEmulatorsDiv = false;            // variable que controla sise muestra el container de los emuladores.
    $scope.showPoweroffQuestionDiv = false;     // Variable que controla si se muestra el container de la pregunta para apagar el dispositivo.
    $scope.showSteamDirectiveDiv = false;       // Variable que controla si se muestra la directiva de steam.
    $scope.showConfigDirectiveDiv = false;      // Variable que controla si se muestra la directiva de la configuracion.
    $scope.showRomsDiv = false;
    
    $rootScope.showWifiDiv = false;
    $rootScope.showAudioDiv = true;
    
    //$scope.showMouseDiv = false;
    //$scope.showKeyboardDiv = false;
    //$scope.showVideoDiv = false;
    //$scope.showTimeDiv = false;

    $rootScope.showWifiLoadingAnim = false;
    $rootScope.showWifiMess = true;
    $rootScope.wifiMessage = "";
    
    $rootScope.showGamepadConfigDiv = false;
    
    $scope.showFooter = true;

    
    /**************************/
    /* CARRGA DATOS INICIALES */
    /**************************/
    
    $scope.initApp = function() {
        //document.getElementById('body').className += 'body-onload';
        //document.getElementById('body').style.opacity = "0.5";
        
        $('#body').addClass('body-onload');
         
    };
    
    $rootScope.ip = "";

    // Devuelve la Ip del servidor para mostrarla en pantalla.
    var getIp = function() {
        $http.get("/getIp").then(function (response) {
            console.log(response);
            $scope.ip= response.data.address[0] + ':8888';
        });
    };
    
    getIp();
    $rootScope.audioVolumen = "0";

    
    /***************/
    /* ANIMACIONES */
    /***************/
    
    var launchAnimation = function(idElement) {

        // Code for Chrome, Safari and Opera
        document.getElementById(idElement).style.WebkitAnimation = "launchAnim 10s";

        // Standard syntax
        document.getElementById(idElement).style.animation = "launchAnim 10s";
        
        $scope.showFooter = false;
        
        // timeout para devolver el footer
        $timeout(showFooterDiv, 10000);
    };
    
    // Funcion para devolver el footer.
    var showFooterDiv = function() {
        $scope.showFooter = true; 
    }
    
    
    /**************/
    /*** SOCKET ***/
    /**************/
    
    // Evento del socket para inciar una pagina en el explorador.
    socket.on('setUrl', function(data) {
        console.log("set URL: " + data.url);
        window.location = data.url;
    });
    
    // Evento del socket para lanzar la animacion cuando se ejecuta una aplicacion externa.
    socket.on('launchAnimation', function(data) {
        console.log("Lanzando la animacion de lanzamiento de una app desde el socket");
        launchAnimation(data.idElement);
    });
    
    // Evento del socket para recibir el codigo de emparejamiento de los dispositivos.
    socket.on('pairingCode', function(data) {
        console.log("Recibiendo el codigo para enlazar los dispositivos:" + data.keyCode);
        var keyCode = data.keyCode;
        $scope.showUserMessage("Introduce el siguiente código en el PC: ", keyCode, 10000);
    });
    
    socket.on('pairingMoonlight', function(data) {
        console.log("Recibe estado del emparejamiento:" + data);

        if(data !== "") {
            $scope.showUserMessage(data, '', 10000);
        } else {
            $scope.showUserMessage("Emparejado Correctamente", '', 10000);   
        }
    });
    
    /**********************************/
    /* FUNCIONES PARA LANZAR LAS APPS */
    /**********************************/
    
    var lanzarKodi = $scope.lanzarKodi = function() {
        $http.post("/kodi").success(function(data) {
            console.log("lanzar kodi - " + data);
            $scope.showUserMessage("Lanzando Kodi...", "", 10000);
        });
    }
    
    var lanzarRetropie = $scope.lanzarRetropie = function() {
        $http.post("/retropie").success(function(data) {
            console.log("lanzar retropie - " + data);
            $scope.showUserMessage("Lanzando Retropie...", "", 10000);
        });
    }
    
    var lanzarNetflix = $scope.lanzarNetflix = function() {
        $http.post('/netflix').success(function(data) {
            console.log("lanzar netflix - " + data);
            $scope.showUserMessage("Lanzando Netflix...", "", 10000);
        });
    }
    
    var lanzarYoutube = $scope.lanzarYoutube = function() {
        $http.post('/youtube').success(function(data) {
            console.log("lanzar youtube - " + data);
            $scope.showUserMessage("Lanzando Youtube...", "", 10000);
        });
    }
    
    var lanzarGoogle = $scope.lanzarGoogle = function() {
        $http.post('/google').success(function(data) {
            console.log("lanzar google - " + data);
            $scope.showUserMessage("Lanzando Google...", "", 10000);
        });
    }
    
    
    // FUNCIONES PARA CONTOLAR LA APLICACION
    
    var poweroff = $scope.poweroff = function() {
        $http.get("/powerOff");
        $scope.showUserMessage("Apangando el dispositivo...", "", 10000);
    };
    
    var restartSystem = $scope.restartSystem = function() {
        $http.get("/restartSystem");
        $scope.showUserMessage("Reiniciando el dispositivo...", "", 10000);
    };
    
    
    /*************************/
    /* CONTROL DE LAS VISTAS */
    /*************************/
    
    var showSteamDirective = $scope.showSteamDirective = function() {
        $scope.showAppsDiv = false;
        $scope.showEmulatorsDiv = false;
        $scope.showPoweroffQuestionDiv = false;
        $scope.showConfigDirectiveDiv = false;
        $scope.showSteamDirectiveDiv = true;
        
        keyEvents.deleteRowsToEnd(1);
        
        $scope.initValues();
    }
    
    var showApps = $scope.showApps = function() {
        $scope.showEmulatorsDiv = false;
        $scope.showPoweroffQuestionDiv = false;
        $scope.showSteamDirectiveDiv = false;
        $scope.showConfigDirectiveDiv = false;
        $scope.showRomsDiv = false;
        $scope.showAppsDiv = true;
        
        keyEvents.deleteRowsToEnd(1);
        
        setAppsButtons();
    }
    
    var showPoweroffQuestion = $scope.showPoweroffQuestion = function() {
        $scope.showEmulatorsDiv = false;
        $scope.showAppsDiv = false;
        $scope.showSteamDirectiveDiv = false;
        $scope.showConfigDirectiveDiv = false;
        $scope.showRomsDiv = false;
        $scope.showPoweroffQuestionDiv = true;
        
         keyEvents.deleteRowsToEnd(1);
        
        //setPoweroffQuestionMenuButtons();
    }
    
    
    var showConfig = $scope.showConfig = function() {
        $scope.showEmulatorsDiv = false;
        $scope.showAppsDiv = false;
        $scope.showSteamDirectiveDiv = false;
        $scope.showPoweroffQuestionDiv = false;
        $scope.showRomsDiv = false;
        $scope.showConfigDirectiveDiv = true;
        
        keyEvents.deleteRowsToEnd(1);
        
        $scope.initConfigMenu();
        
    };
    
    var showEmulators = $scope.showEmulators = function() {
        $scope.showEmulatorsDiv = true;
        $scope.showAppsDiv = false;
        $scope.showSteamDirectiveDiv = false;
        $scope.showPoweroffQuestionDiv = false;
        $scope.showConfigDirectiveDiv = false;
        $scope.showRomsDiv = false;
        
        keyEvents.deleteRowsToEnd(1);
      
        setEmulatorsButtons();
    };
    
    var showEmulatorRoms = $scope.showEmulatorRoms = function() {
        $scope.showEmulatorsDiv = false;
        $scope.showAppsDiv = false;
        $scope.showSteamDirectiveDiv = false;
        $scope.showPoweroffQuestionDiv = false;
        $scope.showConfigDirectiveDiv = false;
        $scope.showRomsDiv = true;
        
        keyEvents.deleteRowsToEnd(1);

    };
    
    
    
    /********************************/
    /* CONFIGURACION DE LOS BOTONES */
    /********************************/
    
    //var setSteamMenuButtons = function() {
    //    keyEvents.addButton(1, 0, 'question-button-focused', 'question-button', 'yesButton', poweroff);
    //    keyEvents.addButton(1, 1, 'question-button-focused', 'question-button', 'noButton', hidePoweroffQuestion);
    //};
    
    var setConfigMenuButtons = function() {
        keyEvents.addButton(0, 0, 'config-button-focus col-xs-2 col-md-2', 'config-button col-xs-2 col-md-2', 'appsStyle', showApps);
        keyEvents.addButton(0, 1, 'config-button-focus col-xs-4 col-md-4', 'config-button col-xs-4 col-md-4', 'emulatorStyle', showEmulators);
        keyEvents.addButton(0, 2, 'config-button-focus col-xs-4 col-md-4', 'config-button col-xs-4 col-md-4', 'configStyle', showConfig);
        keyEvents.addButton(0, 3, 'config-button-focus col-xs-2 col-md-2', 'config-button col-xs-2 col-md-2', 'poweroffStyle', showPoweroffQuestion);
    };
    
    var setPoweroffQuestionMenuButtons = function() {
        keyEvents.addButton(1, 0, 'question-button-focused', 'question-button', 'yesButton', poweroff);
        keyEvents.addButton(1, 1, 'question-button-focused', 'question-button', 'noButton', showApps);
    };
    
    var setAppsButtons = function() {
        keyEvents.addButton(1, 0, 'program-image-focus img-rounded', 'program-image img-rounded', 'kodi-image', lanzarKodi);
        keyEvents.addButton(1, 1, 'program-image-focus img-rounded', 'program-image img-rounded', 'retropie-image', lanzarRetropie);
        keyEvents.addButton(1, 2, 'program-image-focus img-rounded', 'program-image img-rounded', 'steam-image', showSteamDirective);
        keyEvents.addButton(1, 3, 'program-image-focus img-rounded', 'program-image img-rounded', 'netflix-image', lanzarNetflix);
        keyEvents.addButton(2, 0, 'program-image-focus img-rounded', 'program-image img-rounded', 'youtube-image', lanzarYoutube);
        keyEvents.addButton(2, 1, 'program-image-focus img-rounded', 'program-image img-rounded', 'google-image', lanzarGoogle);
    };
    
    var setEmulatorsButtons = function() {
        keyEvents.addButton(1, 0, 'program-image-focus img-rounded', 'program-image img-rounded', 'snes-image', '');
        keyEvents.addButton(1, 1, 'program-image-focus img-rounded', 'program-image img-rounded', 'gba-image', '');
        keyEvents.addButton(1, 2, 'program-image-focus img-rounded', 'program-image img-rounded', 'neogeo-image', '');
        keyEvents.addButton(1, 3, 'program-image-focus img-rounded', 'program-image img-rounded', 'mame-image', '');
        keyEvents.addButton(2, 0, 'program-image-focus img-rounded', 'program-image img-rounded', 'n64-image', '');
        keyEvents.addButton(2, 1, 'program-image-focus img-rounded', 'program-image img-rounded', 'ps-image', '');
        keyEvents.addButton(2, 2, 'program-image-focus img-rounded', 'program-image img-rounded', 'nes-image', '');
    };
    
    
    // Añade los botones principales.
    var setMainMenuButtons = function() {
        setConfigMenuButtons();
        setAppsButtons();
    };
    
    

    setMainMenuButtons();   // Inicializa los botones.
    
    $scope.getKeyEvent = function($event) {
        keyEvents.getKeyEvent($event);   
    };
    
    $scope.disableSelectedButton = function($event) {
        keyEvents.disableSelectedButton($event);  
    };

    
    /*+++++++++++++++++*/
    /*    RETROARCH    */
    /*******************/
    
    $scope.emulatorRoms = null;
    
    $scope.listRoms = function(platform) {
        console.log('Path: ' + platform);
        $http.post('/retroarch/listRoms', { 'platform': platform}).success(function(data) {
            console.log(data);
            $scope.emulatorRoms = {'platform' : platform,
                           'roms' : data
                          };
//            $scope.emulatorRoms = {'platform' : platform,
//                                   'roms' : ['juego1',
//                                             'juego2',
//                                             'juego3',
//                                             'juego4',
//                                             'juego5',
//                                             'juego6',
//                                             'juego7',
//                                             'juego8',
//                                             'juego9',
//                                             'juego10',
//                                             'juego12',
//                                             'juego13',
//                                             'juego14',
//                                             'juego15',
//                                             'juego16',
//                                             'juego17',
//                                             'juego18',
//                                             'juego19',
//                                             'juego20'
//                                            ]
//                          };
            
            $scope.showEmulatorRoms();
        });
    };
    
    
    $scope.launchRom = function(platform, rom) {
        $http.post('/retroarch/launchRom', { 'platform': platform, 'rom': rom}).success(function(data) {
            console.log('data');   
            $scope.showUserMessage('Lanzando juego de ' + platform + ': ' + rom, '', 10000);
        });
    };
    

    
    /****************************/
    /* MENSAJES PARA EL USUARIO */
    /****************************/
    
    $scope.showMess = false;
    $scope.userMessage = "";
    
    $scope.showUserMessage = function(mess, data, time) {
        $scope.userMessage = mess + data;
        $scope.showMess =  true;
        
        // Inicializa el reloj de la pantalla.
        $timeout(hideUserMessage, time);
    }
    
    
    var hideUserMessage = function() {
        $scope.showMess = false;   
    }
    
    
    $scope.switchToVersion = function() {
        console.log("Switching to version 2.0...");
        location.href = "/TV";
    };
    

});