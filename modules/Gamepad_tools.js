var fs = require('fs');
//var Inotify = require('inotify').Inotify;
//var inotify = new Inotify();

var robot   = require('./Robotjs.js');
var logger  = require('./Logger.js');

var gamepadPath = '/dev/input/by-id/';


var Gamepad = function(name, io, plugged, blocked) {
 
    logger.log("info", "Creating one gamepad " + name);
    var segment = null;
    var readStream = fs.createReadStream(gamepadPath + name);
    this.blocked = blocked;
    var configuring = false;  
    var that = this;
    this.gamepadProps = null;
    
    // Variables para configurar los gamepads.
    var buttons = ['UP', 'DOWN', 'LEFT', 'RIGHT', 'A', 'B', 'X', 'Y', 'L', 'R', 'START', 'SELECT'];
    var buttonIndex = 0;
    
    var io = io;


    // Carga la configuracion del gamepad detectado del archivo de configuracion, si no existe configuracion settea una por defecto.
    var loadGamepadConfig = function(name, io) {
  
        logger.log("debug", "Loading gamepads configuration.");
        var data = fs.readFileSync(__dirname + '../gamepad_config.json', "utf8");
        var dataArray = data.split('\n');

        for(var i=0; i<dataArray.length; i++) {
            var obj = null;

            if (dataArray[i] !== "") {
                try {
                    obj = JSON.parse(dataArray[i]);
                }
                catch (err) {
                    logger.log("error", "Error parsing JSON object.");
                    logger.log("error", err);
                }

                if(obj.name == name) {
                    logger.log("debug", "Gamepad configuration loaded succesfuly");
                    that.gamepadProps = obj;
                    logger.log("debug", that.gamepadProps);
                }
            }
        }

        if(that.gamepadProps === null) {
            if(!plugged) {
                logger.log("debug", "Loading default gamepad configuration.");
                that.gamepadProps = { name : name,
                                buttons: {
                                        '0' : 'x',
                                        '1' : 'a',
                                        '2' : 'b',
                                        '3' : 'y',
                                        '4' : 'l',
                                        '5' : 'r',
                                        '6' : '',
                                        '7' : '',
                                        '8' : 'shift',
                                        '9' : 'enter',
                                        '-1' : 'up',
                                        '+1' : 'down',
                                        '-0' : 'left',
                                        '+0' : 'right'
                                    }
                                };
            } else if(plugged) {
                // Inicilaiza la configuracion del gamepad.
                logger.log("debug", "Initializing gamepad configuration.");
                that.gamepadProps = { name : name,
                                      buttons: {'0':'', '1':'', '2':'', '3':'', '4':'', '5':'', '6':'', '7':'', '8':'', '9':'', '-1':'', '+1':'', '-0':'', '+0':'' }
                                    };
                configuring = true;
                io.emit('initGamepadConfig', { button:  buttons[buttonIndex]});
                logger.log("debug", "Gamepad configuration. Button: " + buttons[buttonIndex] + ' , left: ' + Math.abs(buttonIndex - buttons.length));
            };
        }
    }(name, io);
    
    
    // Guarda la configuracion de los mandos en un archivo.
    var saveConfigFile = function() {
        var data = fs.readFileSync(__dirname + '/gamepad_config.json', "utf8");
        var dataArray = data.split('\n');

        logger.log("debug", dataArray.length);
        dataArray[dataArray.length-1] = JSON.stringify(that.gamepadProps);
        
        var file = fs.createWriteStream(__dirname + '/gamepad_config.json');
        dataArray.forEach(function(v) { file.write(v + '\n'); });
        file.end();
    };
    
    // Settea el boton en la tecla correspondiente.
    var setButton = function(gamepadEvents) {
        if(gamepadEvents.pressedButton == 0) return; // Comprueba que el evento sea un pulsado de boton.
            
        // setea el boton.
        if(buttons[buttonIndex].toLowerCase() == "start") { that.gamepadProps.buttons[gamepadEvents.buttonCode] = "enter"; }
        else if(buttons[buttonIndex].toLowerCase() == "select") { that.gamepadProps.buttons[gamepadEvents.buttonCode] = "shift"; }
        else { that.gamepadProps.buttons[gamepadEvents.buttonCode] = buttons[buttonIndex].toLowerCase(); }
        
        
        // Comprueba que el final de los botones.
        buttonIndex++;
        if(buttonIndex >= buttons.length) {
            io.emit('finishGamepadConfig', '');
            looger.log("debbug", "Final of gamepad configuration");
            buttonIndex = 0;
            configuring = false;
            saveConfigFile();
        } else {
            io.emit('nextGamepadButton', { button:  buttons[buttonIndex]});
            logger.log("debug", "Gamepad configuration. Button: " + buttons[buttonIndex] + " , left: " + Math.abs(buttonIndex - buttons.length ));
        }
    };

    
    //{"name":"usb-0810_usb_gamepad-joystick","buttons":{"0":"x","1":"a","2":"b","3":"y","4":"l","5":"r","6":"","7":"","8":"select","9":"start","-1":"up","+1":"down","-0":"left","+0":"right"}}

    // Se encarga de leer cada segmento de bytes que entra en el buffer de entrada correspondiente.
    this.readBuffer = function() {

        var gp = that.gamepadProps;

        readStream.on('data', function(buffer) {
            segment = {
                'var tv_sec':   buffer.readInt16LE(0),
                'tv_usec':      buffer.readInt16LE(2),
                'type':         buffer.readInt16LE(4),
                'code':         buffer.readInt8(6),
                'value':        buffer.readInt8(7)
            };


            if(!that.blocked) {
                var gamepadEvents = checkEvent(segment);
                if(configuring) { setButton(gamepadEvents) }
                else { pressButton(gamepadEvents, gp); }  
            }

        });

        readStream.on('error', function(error) {
            this.emit("end");    
        });

        return this;
    };

    // Simula el pulsado de una tecla asociada a los botones de los gamepads.
    var pressButton = function(events, gamepadProps) {

        if(events.pressedButton == 1) {
            logger.log('Pressing: ' + events.buttonCode + ' - ' + gamepadProps.buttons[events.buttonCode]);
            //robot.pressKey(gamepadProps.buttons[events.buttonCode]);
        }
    };



    // Valida la entrada del buffer y saca los codigos de los botones que se han pulsado.
    var checkEvent = function(segment) {

        var pressedButton   = "";     // 0: suelto, 1: presionado
        var buttonCode      = "";     // Codigo del boton


        // Comprueba si se trata de un boton o un eje.
        if(segment.code == 1) {         // BOTON

            // Comprueba si se pulso o solto el boton.
            if(segment.type == 0) {                 // SUELTA BOTON
                pressedButton = "0";    

            } else if(segment.type == 1) {          // PULSA BOTON
                pressedButton = "1";

            }

            // Guarda el codigo del boton pusaldo.
            buttonCode = segment.value.toString();


        } else if(segment.code == 2) {  // EJE

            if(segment.type == 32767 ) {
                buttonCode = "+";   
                pressedButton = "1";

            } else if(segment.type == -32767) {
                buttonCode = "-";
                pressedButton = "1";
            }


            if(segment.value == 0 || segment.value == 2) {  // X
                buttonCode += "0";

            } else if(segment.value ==1) {                // Y
                buttonCode += "1";
            }   
        }

        return ({'pressedButton': pressedButton, 'buttonCode': buttonCode});
    };


};





/*************/
/** INOTIFY **/
/*************/


var Gamepads = function(io) {
    
    var configGamepad = false;
    var gamepads = new Array();
    var n = 0;                      // numero de gamepads enchufados.
    var io = io;
    var that = this;
    var blocked = false;


    // Metodo para configurar los gamepads.
    var setConfig = function(c) {
        configGamepad = c;  
    };

    // Metodo para bloquear/desbloquear los eventos del gamepad.
    this.setBlocked = function(b) {
        blocked = b;
        for(var i=0; i<n; i++) {
            gamepads[i].blocked = b; 
            logger.log("debug", "Gamepad " + i + " blocked: " + gamepads[i].blocked);
        }
    };
    
    
    // Callback que identifica el tipo de evento para crear o destruir el stream del buffer.
    var checkPlug = function(event) {
        var mask = event.mask;
        var type = "";
        if (event.name) {
            type += event.name;
        } else {
            type += ' ';
        }

        
        if (mask & Inotify.IN_CREATE) {
            if(type.indexOf('joystick') != -1 && type.indexOf('event') == -1) {
                logger.log("debug", type + ' created');
                gamepads.push(new Gamepad(type, io, true, blocked).readBuffer());
                n += 1;
            }


        } else if (mask & Inotify.IN_DELETE) {
            if(type.indexOf('joystick') != -1 && type.indexOf('event') == -1) {
                for(var i=0; i<n; i++) {
                    if(type == gamepads[i].gamepadProps.name) {
                        logger.log("debug", type + ' deleted');
                        gamepads.splice(i);
                        n -= 1;
                    }
                }      
            }
        }
    };
    
    
    // Settea un watch para la carpeta /dev/input/by-id
    var setByidWatch = function() {
          var byid_dir = {
            path:      gamepadPath,
            watch_for: Inotify.IN_ALL_EVENTS,
            callback:  checkPlug
        };

        var byid_watch_descriptor = inotify.addWatch(byid_dir);
    };
    
    // Comprueba si se ha creado/destruido algun gamepad dentro de la carpeta by-id.
    var checkByidFolder = function() {
        fs.readdir(gamepadPath, function(err, items) {
            for (var i=0; i<items.length; i++) {
                if(items[i].indexOf('joystick') != -1 && items[i].indexOf('event') == -1) {
                    logger.log("debug", items[i]);
                    gamepads.push(new Gamepad(items[i], io, false, blocked).readBuffer());
                    n += 1;
                }
            }
        });
           
    };
    
    // Comprueba si se ha creado/destruido la carpeta by-id dentro de la carpeta input.
    var checkInputFolder = function(event) {
        var mask = event.mask;
        var type = "";
        if (event.name) type += event.name;

        if (mask & Inotify.IN_CREATE) {
            if(type.indexOf('by-id') != -1) {
                logger.log("debug", type + ' created');
                checkByidFolder();
                setByidWatch();
            }

        } else if (mask & Inotify.IN_DELETE) {
            if(type.indexOf('by-id') != -1) {
                logger.log("debug", type + ' deleted');
            }
        }
    };

    // Escanea las rutas relativas a los gamepads para detectar la conexion/desconexion y crear/cerrar un bufferstream.
    this.scan = function() {

        
        try {   // Si hay un mando enchufado como minimo escaneamos esta ruta continuamente.
            var stats = fs.lstatSync(gamepadPath);
            if(stats.isDirectory()) {
                checkByidFolder();
                setByidWatch();
            }
        } 
        catch(e) {  // Si no hay un mando enchufado hay que escanear la ruta anterior para saber cuando se enchufa uno.
            logger.log("info", "No gamepads plugged");
        }
        
        var input_dir = {
            path:      '/dev/input/',
            watch_for: Inotify.IN_ALL_EVENTS,
            callback:  checkInputFolder
        };

        var input_watch_descriptor = inotify.addWatch(input_dir);
        

        return that;
    };
    
    
    // Crea el archivo de configuracion de los gamepads para retroarch.
    this.createRetroarchFile = function(path) {
        var file = fs.createWriteStream(path + "retroarch.cfg");
        
        // Escribe la cabecera de configuracion en el archivo.
        file.write("\n# Configuracion creada desde Spin Box para configurar los mandos sustituyendo la configuracion general de retroarch.cfg\n\n");
        file.write(path + "\n\n");
        
        // Escribe los botones de cada player en el archivo.
        for(var i=0; i<n; i++) {
            var player = i+1;
            var buttonsCodes = [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-0', '+0', '-1', '+1' ];
            var retroarchButtons = {
                                 'index' : "input_player" + player + "_joypad_index",
                                 'a' : "input_player" + player + "_a_btn",
                                 'b' : "input_player" + player + "_b_btn",
                                 'x' : "input_player" + player + "_x_btn",
                                 'y' : "input_player" + player + "_y_btn",
                                 'l' : "input_player" + player + "_l_btn",
                                 'r' : "input_player" + player + "_r_btn",
                                 'enter' : "input_player" + player + "_start_btn",
                                 'shift' : "input_player" + player + "_select_btn",
                                 'up' : "input_player" + player + "_up_axis",
                                 'down' : "input_player" + player + "_down_axis",
                                 'left' : "input_player" + player + "_left_axis",
                                 'right' : "input_player" + player + "_right_axis"
                                };
        
            file.write(retroarchButtons['index'] + " = " + i + "\n");
            for(var j=0; j<buttonsCodes.length; j++) {
                if(gamepads[i].gamepadProps.buttons[buttonsCodes[j]] != "") {
                    file.write( retroarchButtons[gamepads[i].gamepadProps.buttons[buttonsCodes[j]]] + " = " + buttonsCodes[j] + "\n");   
                }
                
            }
            
            
            file.write("\n");
            
        }
        
        // Escribe las hotkeys al final de archivo.
        file.write("\n# Configuracion de hotkeys.\n" +
                   "input_enable_hotkey_btn = 8\n" +
                   "input_exit_emulator_btn = 9\n" +
                   "input_save_state_btn = 5\n" +
                   "input_load_state_btn = 4\n" +
                   "input_menu_toggle_btn = 0\n" +
                   "input_state_slot_increase_axis = +0\n" +
                   "input_state_slot_decrease_axis = -0\n"
                  );
    
        file.end();
        
        return (path + "retroarch.cfg ");
    };
    
    
};

module.exports = Gamepads;