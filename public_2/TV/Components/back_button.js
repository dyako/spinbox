app.directive("backButton", function() {

    return {
        restric: 'E',
        template:   ' <style>' +
                    ' .back-button { position: absolute; top: 90vh; right: 0vw; background: {{background}}; color: white; width: 7vw; margin: 0px; padding: 1vw; transition: width 0.2s ease-in-out; } ' +
                    ' .back-button:hover { transition: width 0.2s ease-in-out; width: 6vw; } ' +
                    ' </style> ' +
        
                    ' <div class="back-button" ng-click="goToBack()" role="button"> BACK </div> ',
        controller: 'backButtonCtrl',
        scope: {
            background: '@background',
            location: '@location'
        }
    };

});
    
    
app.controller("backButtonCtrl", function($scope, $location) {
    
    $scope.goToBack = function() {
        $location.path($scope.location);
    };
});
    