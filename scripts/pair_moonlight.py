import subprocess
import threading
import sys

import urllib, urllib2
import json

def loop_lines(iterator):

        for line in iterator:
                if line.find('PIN') != -1:
                        httpRequest(line)

def httpRequest(key):

        keyCode = key[-5:-1]
        url = "http://127.0.0.1:8888/python/pairCode"
        key_json = urllib.urlencode({"keyCode": keyCode})

        req = urllib2.Request(url, key_json)
        rsp = urllib2.urlopen(req)


print(sys.argv[1])

pairing_proc = subprocess.Popen(
                ['stdbuf', '-oL', 'moonlight', 'pair', sys.argv[1]],
                stdout=subprocess.PIPE)

lines_iterator = iter(pairing_proc.stdout.readline, b"")

pairing_thread = threading.Thread(target=loop_lines, args=(lines_iterator,))
pairing_thread.start()