var gamepadConfig = angular.module('gamepadConfig', []);

gamepadConfig.directive('gamepadConfigComponent', function() {
    return {
        restrict: 'EA',
        template: "<div ng-controller='gamepadConfigCtrl' style='position: absolute; width: 70vw; height: 60vh; background: #333333; opacity: 0.95; top: 20vh; left: 15vw;'>" +
                  "<div style='font-size: 2em; text-align: center;'>Configuracion de Gamepad</div>" +
                  "<div style='font-size: 3em; margin-top: 20vh; text-align: center;'>{{ button }}</div></div>",
        controller: "watchCtrl",
    }
});

gamepadConfig.controller('gamepadConfigCtrl', function($scope, $rootScope, socket) {

    $scope.button = "";
//    $scope.buttons = ['UP', 'DOWN', 'LEFT', 'RIGHT', 'A', 'B', 'X', 'Y', 'L', 'R', 'START', 'SLECT'];
//    $scope.buttons = [
//                        { name: 'A', value: '' },
//                        { name: 'B', value: '' },
//                        { name: 'X', value: '' },
//                        { name: 'Y', value: '' },
//        
//                        { name: 'D-PAD UP', value: '' },
//                        { name: 'D-PAD LEFT', value: '' },
//                        { name: 'D-PAD RIGHT', value: '' },
//                        { name: 'D-PAD DOWN', value: '' },
//        
//                        { name: 'START', value: '' },
//                        { name: 'SELECT', value: '' },
//        
//                        { name: 'LEFT SHOULDER', value: '' },
//                        { name: 'RIGHT SHOULDER', value: '' },
//                        { name: 'LEFT TRIGGER', value: '' },
//                        { name: 'RIGHT TRIGGER', value: '' },
//        
//                        { name: 'LEFT ANALOG UP', value: '' },
//                        { name: 'LEFT ANALOG LEFT', value: '' },
//                        { name: 'LEFT ANALOG RIGHT', value: '' },
//                        { name: 'LEFT ANALOG DOWN', value: '' },
//        
//                        { name: 'RIGHT ANALOG UP', value: '' },
//                        { name: 'RIGHT ANALOG LEFT', value: '' },
//                        { name: 'RIGHT ANALOG RIGHT', value: '' },
//                        { name: 'RIGHT ANALOG DOWN', value: '' }
//                        
//                        ];
    
    
    socket.on('initGamepadConfig', function(data) {
        $scope.button = data.button;
        $rootScope.showGamepadConfigDiv = true;
    });
    
    socket.on('nextGamepadButton', function(data) {
        $scope.button = data.button;
    });
    
    socket.on('finishGamepadConfig', function(data) {
        $scope.button = "";
        $rootScope.showGamepadConfigDiv = false;
    });
});