
function Logger(level) {
    
    this.debugLevel = level;
    this.levels = ['debug', 'error', 'warn', 'info'];
    this.colors = ['\x1b[35m%s\x1b[0m', '\x1b[31m%s\x1b[0m', '\x1b[33m%s\x1b[0m', '\x1b[36m%s\x1b[0m'];
    
};

Logger.prototype.log = function(level, message) {
    
    var d = new Date, 
            dformat = [ (d.getMonth()+1),
                    d.getDate(),
                    d.getFullYear()].join('/')+
                    ' ' +
                  [ d.getHours(),
                    d.getMinutes(),
                    d.getSeconds()].join(':');
    
    if (this.levels.indexOf(level) >= this.levels.indexOf(this.debugLevel) ) {
      if (typeof message !== 'string') {
        message = JSON.stringify(message);
      };
      console.log(this.colors[this.levels.indexOf(level)], '['+level+'] - ['+dformat+'] : '+message);
    }
};

module.exports = new Logger('debug');