var web = angular.module("web", []);

web.controller('webMenuCtrl', function($scope, $http, $interval, $location, $userMessage) {
   
    
    $scope.setLocation = function(path) {
        $location.path(path);
    };
    
    
    
    
    /******************/
    /* INIT WEB PAGES */
    /******************/
    
    var webPagesTemp = [];
    var intervalTime = 0;
    
    var getAllWeb = function() {
        $http.post("/db/getDocuments", {collection: 'web'}).then(function (response) {
            webPagesTemp = response.data;
            intervalTime = 1400 / webPagesTemp.length;
            initWebPagesAnim();
        });
    }();
    
    $scope.webPages = [];
    
    // Inicializa la animacion de las webs.
    var initWebPagesAnim = function() {
        var i = 0;
        var pushWebPages = function() {
            $scope.webPages.push(webPagesTemp[i]);
            i++;
            if(i>=webPagesTemp.length) { $interval.cancel(webPagesInterval) };
        };

        var webPagesInterval = $interval(pushWebPages, intervalTime);
    };
    
    // Elimina la clase de la animacion en la inicializacion.
    $interval(function() {
        $(".web-image-init-anim").removeClass("web-image-init-anim");
    }, 1500, true);
    /****************************************************************************************************/

    
    
    $scope.setPage = function(action) {
        window.location = action;
    };

});