var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;

var logger  = require('./Logger.js');

var url = 'mongodb://localhost:27017/';


function Mongodb(dbName) {

    this.url = url + dbName;

    MongoClient.connect(this.url, function(err, db) {
        assert.equal(null, err);
        logger.log("info", "Connected correctly to MongoDB server.");
        db.close();
    });
};

// Metodo que busca documentos en una coleccion.
Mongodb.prototype.find = function(collection, filter, fields, order, limit, callback) {

    MongoClient.connect(this.url, function(err, db) {
        assert.equal(null, err);
        findOnCollection(db, collection, filter, fields, order, limit, function(documents) {
            db.close();
            //logger.log('debug', documents);
            callback(documents);
        });
    });
};

// Metodo que actualiza documentos en una coleccion.
Mongodb.prototype.update = function(collection, filter, update, callback) {

    MongoClient.connect(this.url, function(err, db) {
        assert.equal(null, err);
        updateCollection(db, collection, filter, update, function(results) {
            db.close();
            //logger.log('debug', documents);
            callback(results);
        });
    });
};

// Metodo que inserta un documento en una coleccion.
Mongodb.prototype.insert = function(collection, filter, callback) {

    MongoClient.connect(this.url, function(err, db) {
        assert.equal(null, err);
        insertOnCollection(db, collection, filter, function(documents) {
            db.close();
            //logger.log('debug', documents);
            callback(documents);
        });
    });
};

// Metodo que elimina documentos en una coleccion.
Mongodb.prototype.delete = function(collection, filter, callback) {
    
    MongoClient.connect(this.url, function(err, db) {
        assert.equal(null, err);
        deleteFromCollection(db, collection, filter, function(documents) {
            db.close();
            //logger.log('debug', documents);
            callback(documents);
        });
    });
};





// Funcion que devuelve el resultado de una query FIND.
var findOnCollection = function(db, collection, filter, fields, order, limit, callback) {
    logger.log('debug', 'Finding ' + JSON.stringify(filter) + ' document on "' + collection + '" collection');

    var documents = [];
    
    var cursor = db.collection(collection).find(filter, fields, {sort: order, limit: limit});
    cursor.each(function(err, doc) {
        if(err !== null) { logger.log('error', err); }
        
        if (doc != null) {
            //logger.log('debug', doc);
            documents.push(doc);
        } else {
            callback(documents);
        }
    });

};


var updateCollection = function(db, collection, filter, update, callback) {
    logger.log('debug', 'Updating ' + JSON.stringify(filter) + ' document on "' + collection + '" collection');

    db.collection(collection).update(filter, {$set: update}, function(err, results) {
        if(err !== null) { logger.log('error', err); }
        callback(results);
   });
}


var insertOnCollection = function(db, collection, filter, callback) {
    logger.log('debug', 'Inserting ' + JSON.stringify(filter) + ' document on "' + collection + '" collection');

    db.collection(collection).insertOne(filter, function(err, results) {
        if(err !== null) { logger.log('error', err); }
        callback(results);
    });
};


var deleteFromCollection = function(db, collection, filter, callback) {
    logger.log('debug', 'Deleting ' + JSON.stringify(filter) + ' document on "' + collection + '" collection');

    db.collection(collection).remove(filter, function(err, results) {
        if(err !== null) { logger.log('error', err); }
        callback(results);
    });
};


module.exports = Mongodb;