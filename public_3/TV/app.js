var app = angular.module("spinBox", ['ngRoute', 'watch', 'ngAnimate', 'userMessageComponent']);
            
app.config(function($routeProvider, $locationProvider) {
    
    //$locationProvider.html5Mode(true);
    
    $routeProvider
    .when("/main", {
        templateUrl : "TV/Templates/main_menu.html",
        animation: 'fade'
    })
    .otherwise({
        templateUrl : "TV/Templates/main_menu.html",
    });
    
});
    
    
app.controller("indexCtrl", function($scope, $rootScope, $http, $location) {
    
    $rootScope.version = "3.0";
    $rootScope.ip = "";
    
    $rootScope.animations = true;
    
    $('body').css("overflow", "hidden");

    // Devuelve la Ip del servidor para mostrarla en pantalla.
    var getIp = function() {
        $http.get("/getIp").then(function (response) {
            console.log(response);
            $scope.ip= response.data.address[0];
        });
    }();
    
});