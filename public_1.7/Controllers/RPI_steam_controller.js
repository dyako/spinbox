steamApp.controller('RPIsteamCtrl', function($scope, $http, keyEvents) {
    
    
    $scope.steamOptions = null;

    $http.get("/loadSteamConfig").success(function (data, status) {
        $scope.steamOptions = data
        console.log('Configuracion cargada: ' + $scope.steamOptions);
    });

    if($scope.steamOptions == null) {
        $scope.steamOptions = {
                remoteIP: '0.0.0.0',
                resolution: '1080',
                fps: '60fps',
                paired: false
        };
    };
    
    
    // Funciones definidas para lanzar desde el teclado.
    var set1080 = function() {
        $scope.setActive('resolution', '1080');
    };
    var set720 = function() {
        $scope.setActive('resolution', '720');
    };
    var set60fps = function() {
        $scope.setActive('fps', '60');
    };
    var set30fps = function() {
        $scope.setActive('fps', '30');
    };
    
    
    
    $scope.initValues = function() {
        
        
        keyEvents.addButton(1, 0, 'input-ip-focus', 'input-ip', 'ip', '');
        
        if($scope.steamOptions.resolution == '1080') {
            keyEvents.addButton(2, 0, 'radio-button-select-focus', 'radio-button-select', '1080Radio', set1080);
            keyEvents.addButton(2, 1, 'radio-button-focus', 'radio-button', '720Radio', set720);
        }
        else {
            keyEvents.addButton(2, 0, 'radio-button-focus', 'radio-button', '1080Radio', set1080);
            keyEvents.addButton(2, 1, 'radio-button-select-focus', 'radio-button-select', '720Radio', set720);
        }
        
        if($scope.steamOptions.fps == '60fps') {
            keyEvents.addButton(3, 0, 'radio-button-select-focus', 'radio-button-select', '60fpsRadio', set60fps);
            keyEvents.addButton(3, 1, 'radio-button-focus', 'radio-button', '30fpsRadio', set30fps);
        }
        else {
            keyEvents.addButton(3, 0, 'radio-button-focus', 'radio-button', '60fpsRadio', set60fps);
            keyEvents.addButton(3, 1, 'radio-button-select-focus', 'radio-button-select', '30fpsRadio', set30fps);
        }
        
        keyEvents.addButton(4, 0, 'steam-button-focus', 'steam-button', 'pairButton', pairSteam);
        keyEvents.addButton(4, 1, 'steam-button-focus', 'steam-button', 'streamButton', launchSteam);
    }
    
    var launchSteam = $scope.launchSteam = function() {
        console.log("boton lanzar steam");
        //$scope.saveSteamConfig();
        $http.post("/moonlight/launchProgram", $scope.steamOptions).success(function(data) {
            console.log("lanzar steam - " + data);
            $scope.showUserMessage('Lanzando steam', '', 10000);
        });
    };
                                                          
    
    var pairSteam = $scope.pairSteam = function() {
        console.log("boton de pair");
        //$scope.saveSteamConfig();
        $http.post("/moonlight/pair", $scope.steamOptions).success(function(data) {
            console.log("lanzar pair - " + data);
        });
    };


    $scope.saveSteamConfig = function() {
        console.log("boton guardar configuracion");
        $http.post("/saveSteamConfig", $scope.steamOptions).success(function(data) {
            console.log("Guardando configuracion de steam - " + data);
        });
    };
    
    
    $scope.setActive = function(option, value) {
        if(option == 'resolution') {
            if(value == '1080') {
                keyEvents.changeButtonValues(2, 0, 'radio-button-select-focus', 'radio-button-select', '', '');
                keyEvents.changeButtonValues(2, 1, 'radio-button-focus', 'radio-button', '', '');
                $scope.steamOptions.resolution = '1080';
            }
            else {
                keyEvents.changeButtonValues(2, 1, 'radio-button-select-focus', 'radio-button-select', '', '');
                keyEvents.changeButtonValues(2, 0, 'radio-button-focus', 'radio-button', '', '');
                $scope.steamOptions.resolution = '720';
            }
        }
        
        if(option == 'fps') {
            if(value == '60') {
                keyEvents.changeButtonValues(3, 0, 'radio-button-select-focus', 'radio-button-select', '', '');
                keyEvents.changeButtonValues(3, 1, 'radio-button-focus', 'radio-button', '', '');
                $scope.steamOptions.fps = '60fps';
            }
            else {
                keyEvents.changeButtonValues(3, 1, 'radio-button-select-focus', 'radio-button-select', '', '');
                keyEvents.changeButtonValues(3, 0, 'radio-button-focus', 'radio-button', '', '');
                $scope.steamOptions.fps = '30fps';
            }
        }
        
    };
    
    
    $scope.getPairValue = function() {
        if($scope.steamOptions.paired) {
            return 'PAIRED';   
        }
        else {
            return 'UNPAIRED';   
        }
    };
    
});