steamApp.controller('steamCtrl', function($scope, $http) {

    $scope.hideOptions = true;
    
    $scope.switchHideOptions = function()
    {
        if($scope.hideOptions) {
            $scope.hideOptions = false;   
        }
        else {
            $scope.hideOptions = true;   
        }
    }

    $scope.steamOptions = null;

    $http.get("/loadSteamConfig").success(function (data,status) {
        $scope.steamOptions = data
        console.log('Configuracion cargada: ' + $scope.steamOptions);
    });

    if($scope.steamOptions == null) {
        $scope.steamOptions = {
                remoteIP: '0.0.0.0',
                resolution: '-1080',
                fps: '-60fps',
                paired: false
        };
    };
    
    $scope.launchSteam = function() {
        console.log("boton lanzar steam " + $scope.steamOptions);
        $http.post("/moonlight/launchProgram", $scope.steamOptions).success(function(data) {
            console.log("lanzar kodi - " + data);
        });
    }
                                                          
    
    $scope.pairSteam = function() {
        console.log("boton de pair");
        //$scope.saveSteamConfig();
        $http.post("/moonlight/pair", $scope.steamOptions).success(function(data) {
            console.log("lanzar kodi - " + data);
        });
    }


    $scope.saveSteamConfig = function() {
        console.log("boton guardar configuracion");
        $http.post("/saveSteamConfig", $scope.steamOptions).success(function(data) {
            console.log("lanzar kodi - " + data);
        });
    }
    
    $scope.getActive = function(option, value) {
        if($scope.steamOptions[option] == value) {
            return 'active';   
        }
    };
    
    $scope.getPairValue = function() {
        if($scope.steamOptions.paired) {
            return 'PAIRED';   
        }
        else {
            return 'UNPAIRED';   
        }
    };
    
});