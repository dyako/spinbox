var app = angular.module('remoteMCJS', ['steam', 'kodi', 'retropie', 'netflix', 'mouseControl']);

app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.timeout = 5000;
}]);


app.controller("bodyCtrl", function($scope, $http){
    
    $scope.showAppsDiv = true;
    $scope.showMouseDiv = false;
    
    $scope.showMouseControl = function() {
        
        $scope.showAppsDiv = false;
        $scope.showMouseDiv = true;
      
        $scope.initMouseControl();
    };
    
    $scope.showRemoteControl = function() {
        
        $scope.showAppsDiv = true;
        $scope.showMouseDiv = false;

    };
    

    $scope.apps = [
        {name: 'kodi', active: false},
        {name: 'retropie', active: false},
        {name: 'steam', active: false},
        {name: 'netflix', active: false}
    ];
    
    
    $scope.onclickApp = function(app)
    {
        for(var i = 0; i < $scope.apps.length; i++)
        {   
            if($scope.apps[i].name == app) {
                if($scope.apps[i].active) {
                   $scope. apps[i].active = false;
                }
                else {
                    $scope.apps[i].active = true;
                }
            }
            else {
                $scope.apps[i].active = false;
            }
        }
             
    }


    $scope.powerOff = function() {
           $http.get("/powerOff");
    };
    
    $scope.goHome = function() {
        $http.get("/goHome");  
    };


});
