var button = angular.module('gravityButtonComponent', []);

button.directive('gravityButton', function() {
    return {
        restric: 'E',
        template: "<style>    .box { transition: all 0.5s ease-out; position: relative; z-index: 0; margin: auto; width: {{width}}vw; height: {{height}}vw; box-shadow: {{width*0.03}}vw {{width*0.03}}vw {{width*0.01}}vw {{shadowColor}}; overflow: hidden; }" +
                            " .box:hover { transition: all 0.5s ease-out; transform: scale(1.05); box-shadow: {{width*0.04}}vw {{width*0.04}}vw {{width*0.03}}vw {{shadowColor}}; }" +
                            " .image { position: absolute; z-index: 50; transition: all 0.5s ease-in-out; width: 100%; height: 100%; }" +
                            " .box:hover .image { transition: all 0.5s ease-in-out; overflow: hidden; transform: scale(1.05); }" +
                            " .cover-image { position: absolute; transition: all 0.1s ease-out; z-index: 95; width: 0%; height: 0%; left: 50%; top: 50%; opacity: 0.2; background: white; }" +
                            " .box:hover .cover-image { animation: blink 0.4s ease-out; width: 100%; height: 100%; left: 0%; top: 0%; opacity: 0.2; }" +
                            " .star-div { position: absolute; width: 100%; background: red; z-index: 100; }" +
                            " .arrow-left { transition: all 0.5s ease-out; position: absolute; z-index: 90; width: 0; height: 0; border-bottom: {{width*0.2}}vw solid transparent; border-left: {{width*0.3}}vw solid #888888; }" +      
                            " .box:hover .arrow-left { transition: all 0.2s ease-out; border-bottom: {{width*0.2}}vw solid transparent; border-left: {{width*0.2}}vw solid #888888; }" +
                            " .star-button { transition: all 0.5s ease-out; position: absolute; z-index: 100; width: {{width*0.115}}vw; margin: 0.8%; }" + 
                            " .star-div:hover .star-button{ transition: all 0.4s ease-out; width: {{width*0.16}}vw !important; margin: 0.8% !important; }" +
                            " .star-div:hover .arrow-left { transition: all 0.2s ease-out; border-bottom: 100px solid transparent !important; border-left: 100px solid #888888 !important; }" +       
                            " .box:hover .star-button { transition: all 0.2s ease-out; width: {{width*0.10}}vw; margin: 0.6%; }" +
                            " .image-click { animation: selectClick 0.5s ease-out; } " +
        
                            /* ANIMATIONS */    
                            " @keyframes fadein { 0% { opacity: 0; } 50% { opacity: 0; } 100% { opacity: 1; } }" +
                            " @keyframes blink { 0% {top: 50%; left: 50%; width: 0%; height: 0%;} 90% {top: 50%; left: 50%; width: 0%; height: 0%;} 100% {top: 0%; left: 0%; width: 100%; height: 100%;} }" +
                            " @keyframes selectClick { 0% { overflow: hidden; transform: scale(1); } 50% { overflow: hidden; transform: scale(1.5); } 100% { overflow: hidden; transform: scale(1); } } " +
                            " </style>" +
                            
		                    " <div class='box'>" +
                            "<img id='{{id}}-image-emulator' class='image' src='{{src}}' role='button' ng-click='selectEmulator()'>" +
                            " <div class='star-div' ng-click='toggleTop()'>" +
                            " <div class='arrow-left'></div>" +
                            " <img id='{{id}}-yellow-star' class='star-button' src='images/buttons/star_icon.svg' role='button' ng-if='top==1'>" +
                            " <img id='{{id}}-grey-star' class='star-button' src='images/buttons/star_icon_empty.svg' role='button' ng-if='top==0'>" +
                            " </div></div>",
        
                            
        controller: "gravityButtonCtrl",
        scope: { 
                    id: "=",
                    width: "@width",
                    height: "@height",
                    src: "=",
                    shadowColor: "@shadowColor",
                    top: "=",
                    toggle: "&",
                    select: "&"
               }
    };

});


button.controller('gravityButtonCtrl', function($scope, $interval) {
    
    var yellowStarsNum = 0;
    
    var toggleStars = function() {
        if($scope.top == 1) {
            $scope.top = 0;
            $("#"+$scope.id+"-yellow-star").show();
            $("#"+$scope.id+"-grey-star").hide();
        } else {
            $scope.top = 1;
            $("#"+$scope.id+"-yellow-star").hide();
            $("#"+$scope.id+"-grey-star").show();
        }
    };

    // Funcion que cambia el estado top de un emulador.
    $scope.toggleTop = function() {
        // Llama a la funcion del scope externo. LLamar callback cuando se quiere cambiar estado.
        var top = ($scope.top == 1 ? 0 : 1)
        $scope.toggle({id: $scope.id, top: top, callback: toggleStars});
    };
    
    // Funcion que selecciona el emulador y lanza la pantalla de seleccion de roms.
    $scope.selectEmulator = function() {
        $("#"+$scope.id+"-image-emulator").addClass("image-click");
        
        $interval(function() {
            $("#"+$scope.id+"-image-emulator").removeClass("image-click");
        }, 600, true);
        
        $scope.select({id: $scope.id});
    }
    
    
});