var emulators = angular.module("emulators", []);

emulators.controller('emulatorsMenuCtrl', function($scope, $http, $interval, $location, $userMessage) {
   
    
    $scope.setLocation = function(path) {
        $location.path(path);
    };
    
    
    
    
    /******************/
    /* INIT EMULATORS */
    /******************/
    
    var emulatorsTemp = [
//                    {name: 'Super Nintendo', image: 'images/emulators/snes.png', top: true},
//                    {name: 'Nintendo', image: 'images/emulators/nes.png', top: false},
//                    {name: 'Game Boy Advance', image: 'images/emulators/gba.png', top: false},
//                    {name: 'Neo Geo', image: 'images/emulators/neogeo.png', top: false},
//                    {name: 'Game Boy Color', image: 'images/emulators/gbc.png', top: true},
//                    {name: 'Play Station', image: 'images/emulators/ps.png', top: true},
//                    {name: 'Nintendo 64', image: 'images/emulators/n64.png', top: false}
    ];
    
    var getAllEmulators = function() {
        $http.post("/db/getDocuments", {collection: 'emulators', filter: {}}).then(function (response) {
            emulatorsTemp = response.data;
            
            // Carga los juegos mas jugados de cada emulador.
            emulatorsTemp.forEach(function(item, index) {
                $http.post("/roms/getTopRoms", {filter: {platform: item.id}}).then(function(response) {
                    var roms = response.data;
                    item.topRoms = null;
                    if(roms != null) {
                        item.topRoms = roms;
                    }
                    
                    if(index == emulatorsTemp.length-1) {
                        initShowInfo();         // Inicializa el array que controla la visibilidad de las infos.
                        iniEmulatorsAnim();     // Inicia la animacion de entrada.
                    }
                });
            });
        });
    }();
    
    $scope.emulators = [];
    
    // Inicializa la animacion de las apps.
    var iniEmulatorsAnim = function() {
        var i = 0;
        var pushemulators = function() {
            $scope.emulators.push(emulatorsTemp[i]);
            i++;
            if(i>=emulatorsTemp.length) { $interval.cancel(emulatorsInterval) };
        };

        var emulatorsInterval = $interval(pushemulators, 200);
    };
    /****************************************************************************************************/
    
    
    
    
    /*****************/
    /* EMULATOR INFO */
    /*****************/
    
    // Controla la visibilidad de cada info de los emuladores.
    var showEmulatorsInfo = {};
    
    // Inicializa la info de cada emulador.
    var initShowInfo = function() {
        for(var i=0; i<emulatorsTemp.length; i++) {
            var id = emulatorsTemp[i].id;
            if(i == 0) {
                showEmulatorsInfo[id] = true;
            } else {
                showEmulatorsInfo[id] = false;
            }
        }
    };
    
    // Funcion que devuelve la info del emulador correspondiente.
    $scope.getShowInfo = function(id) {
        return showEmulatorsInfo[id];
    };
    
    // Funcion que cambia la informacion del emulador.
    $scope.setShowInfo = function(id) {
        
        for (var property in showEmulatorsInfo) {
            if (showEmulatorsInfo.hasOwnProperty(property)) {
                showEmulatorsInfo[property] = false;
            }
        }
        showEmulatorsInfo[id] = true;
    };
    /***********************************************************/
    
    
    
    
    
    // Funcion que cambia el estado del top.
    $scope.toggleTop = function(id, top, callback) {
        
        // Valida que solo haya 3 emuladores como preferentes.
        var topNum = 0;
        if(top == 1) {
            for(var i=0; i<$scope.emulators.length; i++) {
                if($scope.emulators[i].top == 1) { topNum++; }
                if(topNum == 3) { 
                    $userMessage.showMessage("You can't set more than 3 preferred emulators. You need remove one before");
                    return; 
                }
            }
        }
        
        // Persiste los cambios en la BBDD.
        var filter = ({id: id, top: top});
        $http.post("/emulators/toggleTop", {filter: filter}).then(function(res) {
            var data = res.data;
            if(data.error == "0") {
                callback();
                
            } else {
                // Error cambiando el estado de preferencia de los emuladores
                $userMessage.showMessage("Error on DB modifying prefered emulators. Try again please");
            }
        });
    };
    
    // Funcion que selecciona el emulador correspondiente.
    $scope.selectEmulator = function(id) {
        console.log("Selecciona emulador: " + id);
        $location.path("/roms/"+id);
    };
    
});