kodiApp.controller('kodiCtrl', function($scope, $http) {
    
    $scope.hideOptions = true;
    
    $scope.switchHideOptions = function()
    {
        if($scope.hideOptions) {
            $scope.hideOptions = false;   
        }
        else {
            $scope.hideOptions = true;   
        }
    }

    $scope.launchKodi = function() {
        console.log("boton lanzar kodi");
        $http.post("/kodi").success(function(data) {
            console.log("lanzar kodi - " + data);
        });
    };
    
});