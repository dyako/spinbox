var configMenu = angular.module('configMenu', []);

configMenu.directive('configMenu', function() {
    return {
        restrict: 'EA',
        templateUrl: 'Templates/RPI_config_menu_template.html',
        controller: 'configMenuCtrl'
    }
});