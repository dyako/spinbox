var watch = angular.module('watch', []);

watch.directive('watchComponent', function() {
    return {
        restrict: 'E',
        template: "<div class='watch-div'>" +
                  "<div class='date' ng-show='{{showDate}}'>{{ clock | date:'fullDate' }}</div>" +
                  "<div class='time'><span>{{ hour }}</span><span>:{{ minutes }}</span></div>" +
                  "</div>",
        controller: "watchCtrl",
        scope: {
                 showDate: "@showDate"
        }
    };
});

watch.controller('watchCtrl', function($scope, $timeout) {
    
    var pad = function pad(num, size) {
        var s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    };
    
    $scope.clock = new Date();
    $scope.tickInterval = 1000; //ms
    
    $scope.hour = pad($scope.clock.getHours(), 2);
    $scope.minutes = pad($scope.clock.getMinutes(), 2);

    var tick = function() {
        $scope.clock = new Date() // get the current time
        $scope.hour = pad($scope.clock.getHours(), 2);
        $scope.minutes = pad($scope.clock.getMinutes(), 2);
        $timeout(tick, $scope.tickInterval); // reset the timer
    };

    // Inicializa el reloj de la pantalla.
    $timeout(tick, $scope.tickInterval); 
});