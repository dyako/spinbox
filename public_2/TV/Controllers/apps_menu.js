app.controller('appsMenuCtrl', function($scope, $interval, $location) {
   
    var appsTemp = [
                    {name: 'kodi', image: 'images/kodi.png'},
                    {name: 'retropie', image: 'images/retropie.png'},
                    {name: 'retropie', image: 'images/retropie.png'},
                    {name: 'retropie', image: 'images/retropie.png'}
    ];
    
    $scope.apps = [];
    
    // Inicializa la animacion de las apps.
    var iniAppsAnim = function() {
        var i = 0;
        var pushapps = function() {
            $scope.apps.push(appsTemp[i]);
            i++;
            if(i>=appsTemp.length) { $interval.cancel(appsInterval) };
        };

        var appsInterval = $interval(pushapps, 200);
    }();
    
    $scope.setLocation = function(path) {
        $location.path(path);
    };
});