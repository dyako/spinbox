
// var robotjs = require("robotjs");

function Robot() {

    //Speed up the mouse.
    robotjs.setMouseDelay(2);

    // Tamaño de la pantalla.
    var screenSize = robotjs.getScreenSize();
    var height = screenSize.height;
    var width = screenSize.width;

};


Robot.printPrueba = function(msg) {
    console.log("----- PRUEBA ------------ :" + msg.x + ', ' + msg.y);  
};


/***********************/
/**** MOUSE CONTROL ****/
/***********************/


// Mueve el raton un offset de cada eje.
Robot.moveMouse = function(data) {
    
    var mouse = robotjs.getMousePos();
    var xPos =  mouse.x + data.x;
    if(xPos > this.width) { xPos = this.width; }
    var yPos = mouse.y - data.y;
    if(yPos > this.height) { yPos = this.height; }
    
    
    robotjs.moveMouse(xPos, yPos);
};


// Controla los clicks del raton.
Robot.clickMouse = function() {
    robotjs.mouseClick();
};


// controla el scroll del raton.
Robot.scrollMouse = function(data) {
    robotjs.scrollMouse(data.magnitude, data.direction);
};

/*********************************/




/****************************/
/***** KEYBOARD CONTROL *****/
/****************************/
// Introduce texto.
Robot.typeText = function(data) {
  
    robotjs.typeString(data.text);
};

// Presiona una tecla
Robot.pressKey = function(button) {
    robotjs.keyTap(button);  
};


module.exports = Robot;