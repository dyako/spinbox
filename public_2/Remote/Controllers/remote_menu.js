app.controller("remoteMenuCtrl", function($scope, $http, $interval, $location) {
    
    // Opciones del menu principal.
    $scope.optionsTemp = [];
    $scope.options = [];
    
    // Recupera las opciones del menu principal.
    var getMainOptions = function() {
        $http.post("/db/getDocuments", {collection: "menu_options"}).then(function (response) {
            $scope.optionsTemp = response.data; 
            initMainOptionsAnim();
        });
    }();
    
    // Inicializa la animacion de los main options.
    var initMainOptionsAnim = function() {
        var i = 0;
        var pushMainOptions = function() {
            $scope.options.push($scope.optionsTemp[i]);
            i++;
            if(i>=4) { $interval.cancel(optionsInterval) };
        };

        var optionsInterval = $interval(pushMainOptions, 100);
    };
        
    $scope.setLocation = function(path) {
        $location.path(path);
    };
});