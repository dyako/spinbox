var button = angular.module('pulseButtonComponent', []);

button.directive('pulseButton', function() {
    return {
        restric: 'E',
        template: "<style>" +
		              ".box { z-index: 100; margin: auto; position: relative; width: {{width}}vw; height: {{height}}vw; border-radius: 50%; margin-top: {{height*0.2}}vw; margin-bottom: {{height*0.2}}vw;" +
                            " transition: all 0.4s ease-in-out; transform: scale(1); }" +

		              ".box:hover { transition: all 0.4s ease-in-out; width: {{width*1.1}}vw; height: {{height*1.1}}vw; margin-top: {{ (height*0.2)-(((height*1.1)-height))}}vw; margin-bottom: {{ (height*0.2)-(((height*1.1)-height))}}vw;}" +

		              ".image { position: absolute; border-radius: 50%; width: 100%; height: 100%; left: 0px;}" +

		              ".cover-image { left: 55%; top: 55%; height: 0%; width: 0%; }" +

		              ".box:hover .cover-image { left: -1%; top: -1%; height: 102%; width: 102%; animation: growUpRadius 0.6s ease-in-out; }" +

		              ".text { vertical-align: middle; text-align: center; color: {{clr}}; font-size: {{fontSize}}vw; margin-top: 25%; opacity: 0; width: 100%; height: 100%; text-shadow: 2px 2px #777777; }" +

		              ".box:hover .text { animation: fadein 0.8s linear; opacity: 1; }" +

		              ".cover-pulse { left: 55%; top: 55%; width: 0%; height: 0%; position: absolute; border-radius: 50%; left: 0px; background: {{bg}}; }" +

		              ".box:hover .cover-pulse { animation: pulse 2s ease-in-out infinite; }" +

		              /* Clase de animacion de mouse leave */
		              ".growDownRadius { transition: all 0.3s ease-out; left: 55%; top: 55%; height: 0%; width: 0%; }" +

		              ".click { transition: all 0.5s ease-out; transform: scale(0); }" +
                      ".resetClick { transition: all 0.5s ease-out; transform: scale(1); }" +


		              /*** ANIMATIONS ***/
		              "@keyframes growUpRadius { 0% { left: 55%; top: 55%; height: 0%; width: 0%; }" +
                                               "50% { left: 55%; top: 55%;	height: 0%;	width: 0%; }" +
			                                  "100% { left: 0%; top: 0%; height: 100%; width: 100%;	}}" +

		              "@keyframes fadein { 0% { opacity: 0; } 70% { opacity: 0; } 100% { opacity: 1; }}" +

		              "@keyframes pulse { 0% { opacity: 0.7; left: 55%; top: 55%; height: 0%; width: 0%; }" +
			                            "30% { left: 55%; top: 55%;	height: 0%; width: 0%; }" +
			                            "50% { opacity: 0.7; left: -1%; top: -1%; height: 102%; width: 102%; }" +
			                           "100% { opacity: 0; left: -1%; top: -1%; height: 102%; width: 102%; }}" +	
	                   "</style>" +
        
                       "<div id='{{id}}-box' class='box' ng-click='clickButton()' role='button' ng-mouseenter='setShadowBox()'  ng-mouseleave='removeShadowBox()'>" +
			           "<img id='{{id}}-image' class='image' ng-src='{{source}}' role='button'>" +
			           "<div id='{{id}}-cover' class='image cover-image'><div id='{{id}}-text' class='text'>{{text}}</div></div>" +
			           "<div id='{{id}}-pulse' class='cover-pulse'></div></div>",
                        
        controller: "pulseButtonCtrl",
        scope: { id: "=",
                 source: "=",
                 width: "@width",
                 height: "@height",
                 shadow: "@shadow",
                 shadowColor: "=",
                 text: "=",
                 fontSize: "=",
                 clr: "@clr",
                 bg: "=",
                 pulseColor: "=",
                 action: "=",
                 type: "="
                
               }
    };

});


button.controller('pulseButtonCtrl', function($scope, $http, $interval) {
    
    $(".box").ready(function() {

        // Settea la clase de la animacion de hacer disminuir el radio del circulo.
        $(".box").mouseleave(function() {
            $(".cover-image").addClass('growDownRadius');

        });
    
        // No se inicializan correctamente los colores parametrizados, por lo que tenemos que settearlos de nuevo otra vez.
        var initColor = function() {
            $('#'+$scope.id+'-cover').css('background', $scope.bg);
            $('#'+$scope.id+'-text').css('font-size', $scope.fontSize+'vw');
            $('#'+$scope.id+'-pulse').css('background', $scope.pulseColor);
            $('#'+$scope.id+'-box').css('box-shadow', $scope.shadow+'vh ' + $scope.shadow+'vh ' + $scope.shadow+'vh ' + $scope.shadowColor);
            
            $interval.cancel(initColorInterval);
        };
        
        var initColorInterval = $interval(initColor, 100);
        
        
        //Funcion para aumentar el tamaño de la sombra del box, por css no lo hace correctamente.
        $scope.setShadowBox = function() {
            $('#'+$scope.id+'-box').css('box-shadow', $scope.shadow*2+'vh ' + $scope.shadow*2+'vh ' + $scope.shadow*2+'vh ' + $scope.shadowColor);
        };
        
        //Funcion para disminuir el tamaño de la sombra del box, por css no lo hace correctamente.
        $scope.removeShadowBox = function() {
            $('#'+$scope.id+'-box').css('box-shadow', $scope.shadow+'vh ' + $scope.shadow+'vh ' + $scope.shadow+'vh ' + $scope.shadowColor);
        };
    });

    
    // Hace que el boton se encoja.
    $scope.clickButton = function(type, action) {
        $('#'+$scope.id+'-box').addClass('click');
        $('#'+$scope.id+'-image').addClass('click');
        $('#'+$scope.id+'-cover').addClass('click');
        $('#'+$scope.id+'-pulse').addClass('click');
        
        if($scope.type == "app") {
            console.log("Lanzando " + $scope.action);
            $http.post($scope.action).then(function(data) {
                console.log("Cierra " + $scope.action);
            });
            
        } else if($scope.type == "moonlight") {
            
        } else if($scope.type == "web") {
            window.location = $scope.action;
            
        } else if($scope.type == "emulator") {
            
        }
        
        
        $interval(function() { 
            $('#'+$scope.id+'-box').removeClass('click');
            $('#'+$scope.id+'-image').removeClass('click');
            $('#'+$scope.id+'-cover').removeClass('click');
            $('#'+$scope.id+'-pulse').removeClass('click');

            $('#'+$scope.id+'-box').addClass('resetClick');
            $('#'+$scope.id+'-image').addClass('resetClick');
            $('#'+$scope.id+'-cover').addClass('resetClick');
            $('#'+$scope.id+'-pulse').addClass('resetClick');
        }, 500);
    }
    
});