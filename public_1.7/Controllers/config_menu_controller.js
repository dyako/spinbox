configMenu.controller('configMenuCtrl', function($scope, $rootScope, $http, keyEvents) {

        
    
    $scope.initConfigMenu = function() {
        setConfigMenuButtons();
    };
    
    //$scope.showWifiMess = false;
    //$scope.wifiMessage = "";
    
    $scope.wifis = [
//        {essid: 'wifi1', bssid: 'A4:34:69:C2:16:AF', power: '-31dB', passwd: 'asjdfg8asdm', channel: '5', enc: 'WPA'},
//        {essid: 'wifi2', bssid: 'A4:34:69:C2:16:AF', power: '-12dB', passwd: 'EDGSDGSDBG', channel: '1', enc: 'WPA/2'},
//        {essid: 'wifi3', bssid: 'A4:34:69:C2:16:AF', power: '-63dB', passwd: 'FD46Dth56443', channel: '8', enc: 'WPA/2'},
//        {essid: 'wifi4', bssid: 'A4:34:69:C2:16:AF', power: '-83dB', passwd: '234232342334200234', channel: '11', enc: 'WPA'},
//        {essid: 'wifi5', bssid: 'A4:34:69:C2:16:AF', power: '-31dB', passwd: 'asjdfg8asdm', channel: '5', enc: 'WPA'},
//        {essid: 'wifi6', bssid: 'A4:34:69:C2:16:AF', power: '-12dB', passwd: 'EDGSDGSDBG', channel: '1', enc: 'WPA/2'},
//        {essid: 'wifi7', bssid: 'A4:34:69:C2:16:AF', power: '-63dB', passwd: 'FD46Dth56443', channel: '8', enc: 'WPA/2'},
//        {essid: 'wifi8', bssid: 'A4:34:69:C2:16:AF', power: '-83dB', passwd: '234232342334200234', channel: '11', enc: 'WPA'}
    ];
    
    $scope.wifiConnected = null;
    $scope.passwdType = 'password';
    
    $rootScope.showWifiOptions = function() {
        //console.log('show wifi options');
        $scope.showAudioDiv = false;
        $scope.showWifiDiv = true;
        
        $scope.scanWifi();
    };

    
    $rootScope.showAudioOptions = function() {
        //console.log('show audio options');
        $scope.showWifiDiv = false;
        $scope.showAudioDiv = true;
    };
    
    
    // Cambia el ocultamiento de los input de las contraseñas.
    $scope.switchPasswdType = function(essid) {
        //console.log('cambia valor de input password ' + essid);
        
        var checkbox    = document.getElementById('passwd-check-'+essid);
        var input       = document.getElementById('passwd-input-'+essid);
        
        if(checkbox.checked) {
            input.type = 'text';
        } else {
            input.type = 'password';
        }
    };

    
    // CONFIGURACION DE WIFI
    
    $scope.scanWifi = function() {
        //$rootScope.wifiMessage = "Buscando redes...";
        //$rootScope.showWifiMess = true;
        $rootScope.showWifiLoadingAnim = true;
        $scope.wifis = null;
        //console.log($scope.showWifiMess + ' , ' + $scope.wifiMessage);
        
        $http.get('/config/wifi/scan').success(function(data) {

            $rootScope.showWifiLoadingAnim = false;
            
            if(data.length > 0) {
                console.log('scan completado');
                $scope.wifis = data;
                
                $scope.getWifi();
            }
            else { // Hay un error
                console.log('error en scan');
                //$rootScope.showWifiMess = true;
                //$rootScope.wifiMessage = "Error buscando redes!!";
                
                $scope.showUserMessage('Error buscando redes', '', 10000);
            }

        });  
    };
    
    
    $scope.connectWifi = function(wifi) {
        console.log('conectando WIFI... ' + wifi.essid);
        
        $rootScope.wifiMessage = "Conectando red " + wifi.essid + " ...";
        //$rootScope.showWifiMess = true;
        $rootScope.showWifiLoadingAnim = true;
        $scope.showUserMessage('Conectando a la red ', wifi.essid, 10000);
        $http.post('/config/wifi/connect', wifi).success(function(data) {
            console.log(data);
            
            $rootScope.showWifiLoadingAnim = false;
            
            //$rootScope.showWifiMess = true;
            $scope.getWifi(true);
            
        });
    };
    
    $scope.getWifi = function(conect) {
        console.log('Devolviendo la wifi');
        
        $http.get('/config/wifi/getWifi').success(function(data) {
            $scope.wifiConnected = data;
            $rootScope.ip = $scope.wifiConnected.ip;
            
            console.log('Ip: ' + $scope.wifiConnected.ip);
            
            if(conect) {
                if($scope.wifiConnected.ip != '') {
                    $scope.showUserMessage('Conectado correctamente a la red ', $scope.wifiConnected.essid, 10000);
                } else {
                    $scope.showUserMessage('Error conectando a la red ', $scope.wifiConnected.essid, 10000);
                }
            }
        });
    };
    
    
    // CONFIGURACION DE AUDIO
    
    $scope.getVolumen = function() {
        $http.get('/config/audio/getVolumen').success(function(data) {
            console.log('Getting Audio ' + data)   
            $rootScope.audioVolumen = data;
        });
    };
    
    
    $scope.setVolumen = function() {
        $http.post('/config/audio/setVolumen', { volume : $scope.audioVolumen}).success(function(data) {
            console.log('Setting audio');
            console.log(data);
        });
    };
    
    
    // CONTROL DEL TECLADO
    
    var setConfigMenuButtons = function() {
        keyEvents.addButton(1, 0, 'sub-config-button-focused col-xs-2 col-md-2', 'sub-config-button col-xs-2 col-md-2', 'wifi-button', $rootScope.showWifiOptions);
        keyEvents.addButton(1, 1, 'sub-config-button-focused col-xs-2 col-md-2', 'sub-config-button col-xs-2 col-md-2', 'audio-button', $rootScope.showAudioOptions);
    };
    
    
});